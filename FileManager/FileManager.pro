QT       += core gui network charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    sources/connectiondialog.cpp \
    sources/ftp_manager.cpp \
    sources/main.cpp \
    sources/mainwindow.cpp \
    sources/preview.cpp \
    sources/window.cpp \
    sources/selectedfileslist.cpp \
    sources/file.cpp \
    sources/contextmenu.cpp \
    sources/propertiesdialog.cpp \
    sources/clipboard.cpp \
    sources/http_client.cpp\
    sources/filesystemmodel.cpp

HEADERS += \
    headers/connectiondialog.h \
    headers/ftp_manager.h \
    headers/mainwindow.h \
    headers/preview.h \
    headers/window.h \
    headers/selectedfileslist.h \
    headers/file.h \
    headers/contextmenu.h \
    headers/propertiesdialog.h \
    headers/clipboard.h \
    headers/http_client.h \
    headers/filesystemmodel.h

#zip

QUAZIPCODEDIR =  $${_PRO_FILE_PWD_}"/libs/quazip"
ZLIBCODEDIR = $${_PRO_FILE_PWD_}"/libs"

LIBS += -L$${ZLIBCODEDIR} -lz

INCLUDEPATH += $${QUAZIPCODEDIR}
HEADERS += $${QUAZIPCODEDIR}/*.h
SOURCES += $${QUAZIPCODEDIR}/*.cpp
SOURCES += $${QUAZIPCODEDIR}/*.c


############

FORMS += \
    forms/connectiondialog.ui \
    forms/ftp_manager.ui \
    forms/mainwindow.ui \
    forms/http_client.ui \
    forms/propertiesdialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

SUBDIRS += \
    FileManager.pro

RESOURCES += \
    resources/resources.qrc


