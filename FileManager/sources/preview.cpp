#include "headers/preview.h"
#include "headers/selectedfileslist.h"
#include <QDebug>

Preview::Preview(QGraphicsView *graphicsView)
    : m_graphicsView(graphicsView)
{
    m_w=m_graphicsView->width();
    m_h=m_graphicsView->height();

    m_graphicsView->setMaximumSize(QSize(250,300));
    m_graphicsView->setVisible(false);
}

QGraphicsView* Preview::getGraphicsView()
{
    return m_graphicsView;
}

void Preview::setScene()
{
    if(!(m_graphicsView->scene())){
        QGraphicsScene *scene=new QGraphicsScene(this);
        this->m_graphicsView->setScene(scene);
     }
    else{
        m_graphicsView->scene()->clear();
    }
}

void Preview::imagePreview(const File file)
{
    if(!file.isImg()) {
        m_graphicsView->setVisible(false);
        return;
    }

    setScene();

    m_graphicsView->setVisible(true);

    QPixmap p(file.getPath());
    m_w=m_graphicsView->width();
    m_h=m_graphicsView->height();
    m_graphicsView->scene()->addPixmap(p.scaled(m_w,m_h,Qt::KeepAspectRatio));
}

QChart* Preview::displayPieChart(const SelectedFilesList* leftList, const SelectedFilesList* rightList)
{
    QPieSeries *series = new QPieSeries();

    QMap<QString,int> extMap;
    QMap<QString,int> extMap2;
    if(leftList->isEmpty()){
        extMap2=extensionsMap(rightList);
    }
    else if(rightList->isEmpty()){
        extMap2=extensionsMap(leftList);
    }
    else{
        extMap=extensionsMap(leftList);
        extMap2=extensionsMap(rightList);
    }

    QString ext;
    int m;
    int n;
    QMap<QString,int>::iterator i;
    for(i=extMap2.begin();i!=extMap2.end();i++){
        ext=i.key();
        m=i.value();
        if(extMap.contains(ext)){
            n=extMap.value(ext);
            extMap.remove(ext);
            n+=m;
            extMap.insert(ext,n);
        }
        else{
            extMap.insert(ext,m);
        }
    }

    for(i=extMap.begin();i!=extMap.end();i++){
        series->append(i.key(),i.value());
    }

    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->setTitle("Extensions Pie Chart:");
    chart->legend()->setAlignment(Qt::AlignLeft);
    return chart;
}

QMap<QString, int> Preview::extensionsMap(const SelectedFilesList *list)
{
    QMap<QString,int> extMap;
    for(File file : list->toStdList())
    {
        processFile(file, extMap);
    }
    return extMap;
}

void Preview::processFile(const File file, QMap<QString, int> &extMap) const
{
    QString ext;
    int n;
    if(file.isDir())
    {
        processDirectory(file.getPath(), extMap);
    }
    else
    {
        ext = file.getExtension();
        if(extMap.contains(ext)){
            n=extMap.value(ext);
            extMap.remove(ext);
            qDebug()<<file.getSize()<<" "<<file.getName();
            n += file.getSize();
            extMap.insert(ext,n);
        }
        else{
            qDebug()<<file.getSize()<<" "<<file.getName();
            extMap.insert(ext, file.getSize());
        }
    }
}

void Preview::processDirectory(QString path, QMap<QString, int> &extMap) const
{
    QDir dir(path);
    for(const QFileInfo &fileInfo : dir.entryInfoList(QDir::Files|QDir::NoDotAndDotDot|QDir::AllDirs)){
        File file(fileInfo);
        processFile(file, extMap);
    }
}
