#include "headers/mainwindow.h"
#include "ui_mainwindow.h"
#include "headers/connectiondialog.h"
#include "headers/filesystemmodel.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_ui(new Ui::MainWindow)
{
    m_clipboard = new Clipboard();

    QFile styleFileInit( ":/stylesheets/lightTheme.qss" );
    styleFileInit.open( QFile::ReadOnly );
    QString style( styleFileInit.readAll() );
    this->setStyleSheet( style );

    m_ui->setupUi(this);

    m_dragAndDropAction = DragAndDropAction::MOVE_ACTION;

    m_leftWindow = new Window(this, new FileSystemModel(this, &m_dragAndDropAction), m_ui->tvLeftWindow, m_ui->lePathLeft);
    m_rightWindow = new Window(this, new FileSystemModel(this, &m_dragAndDropAction), m_ui->tvRightWindow, m_ui->lePathRight);

    m_preview=new Preview(m_ui->gvPreview);
    m_viewEnabled=false;

    m_openWithFunctionEnabled = false;
    QAction* viewAction = new QAction(this);
    connect(viewAction, &QAction::triggered, this, &MainWindow::on_pbViewClicked);
    viewAction->setShortcut(QKeySequence(Qt::Key_F2));
    this->addAction(viewAction);

    m_isMove=false;

    m_pathCopiedFrom="";

    connect(m_ui->action_darkMode, &QAction::changed,
            this, &MainWindow::toggle_darkMode);
    
    connect(qApp, SIGNAL(focusChanged(QWidget*,QWidget*)),
            this, SLOT(on_FocusChanged(QWidget*,QWidget*)));


    connect(m_ui->action_showHiddenFilesDirectories, &QAction::changed,
            this, &MainWindow::toggle_ShowHiddenFilesAndDirectories);

    connect(m_ui->action_previewImage, &QAction::changed,
            this, &MainWindow::toggle_imagePreview);

    connect(m_ui->action_alternatingRowColors, &QAction::changed,
            this, &MainWindow::toggle_alternatingRowColors);

    connect(m_ui->action_enableOpenWithFunction, &QAction::changed,
            this, &MainWindow::toggle_openWithFunction);

    connect(m_ui->action_setCopyAction, &QAction::triggered,
            this, &MainWindow::changeDragAndDropAction);

    connect(m_ui->action_setMoveAction, &QAction::triggered,
            this, &MainWindow::changeDragAndDropAction);

    connect(m_ui->tvLeftWindow, &QAbstractItemView::clicked,
            this, &MainWindow::clickedTvLeft);

    connect(m_ui->tvRightWindow, &QAbstractItemView::clicked,
            this, &MainWindow::clickedTvRight);

    connect(m_ui->pbView, &QAbstractButton::pressed,
            this, &MainWindow::on_pbViewClicked);

    connect(m_ui->pbDelete,&QPushButton::clicked,
            this, &MainWindow::deleteFiles);
}

MainWindow::~MainWindow()
{
    delete m_ui;
}

void MainWindow::keyPressEvent(QKeyEvent *e) {
    if(e->type() == QKeyEvent::KeyPress) {
        if(e->matches(QKeySequence::Paste)) {
            pasteFiles();
        }else if(e->matches(QKeySequence::Copy)) {
            copyFiles();
        }else if(e->matches(QKeySequence::Delete) || e->key()==Qt::Key_F8){
            deleteFiles(true);
        }else if(e->matches(QKeySequence::Cut)){
            moveFiles();
        }else if(e->matches(QKeySequence::New)  || e->key()==Qt::Key_F7){
            on_pbNewFolder_clicked();
        }else if(e->key()==Qt::Key_F2){
            on_pbViewClicked();
        }
    }
}

Clipboard* MainWindow::getClipboard() const
{
    return m_clipboard;
}

bool MainWindow::isOpenWithFunctionEnabled() const
{
    return m_openWithFunctionEnabled;
}

void MainWindow::toggle_ShowHiddenFilesAndDirectories()
{
    m_leftWindow->toggle_ShowHiddenFilesAndDirectories();
    m_rightWindow->toggle_ShowHiddenFilesAndDirectories();
}

void MainWindow::toggle_alternatingRowColors()
{
    bool toggle = m_ui->action_alternatingRowColors->isChecked();
    m_leftWindow->toggle_alternatingRowColors(toggle);
    m_rightWindow->toggle_alternatingRowColors(toggle);
}

void MainWindow::toggle_imagePreview()
{
    m_preview->getGraphicsView()->setVisible(m_viewEnabled);
}

void MainWindow::toggle_openWithFunction()
{
    m_openWithFunctionEnabled = !m_openWithFunctionEnabled;
}

void MainWindow::toggle_darkMode()
{
    QFile *styleFile = m_ui->action_darkMode->isChecked()
            ? new QFile(":/stylesheets/darkTheme.qss")
            : new QFile(":/stylesheets/lightTheme.qss");

    styleFile->open( QFile::ReadOnly );
    QString style( styleFile->readAll() );
    this->setStyleSheet( style );
}

void MainWindow::changeDragAndDropAction()
{
    if(sender() == m_ui->action_setCopyAction)
    {
        m_ui->action_setMoveAction->toggle();
        m_dragAndDropAction = DragAndDropAction::COPY_ACTION;
    }
    else
    {
        m_ui->action_setCopyAction->toggle();
        m_dragAndDropAction = DragAndDropAction::MOVE_ACTION;
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    m_settings.setValue("geometry", saveGeometry());
    m_settings.setValue("windowState", saveState());

    m_settings.setValue("leftDirectory", m_leftWindow->getDirectory());
    m_settings.setValue("rightDirectory", m_rightWindow->getDirectory());

    m_settings.setValue("showHidden", m_ui->action_showHiddenFilesDirectories->isChecked());
    m_settings.setValue("altRows", m_ui->action_alternatingRowColors->isChecked());
    m_settings.setValue("darkMode", m_ui->action_darkMode->isChecked());
    m_settings.setValue("previewImage", m_ui->action_previewImage->isChecked());

    m_settings.setValue("dragAndDropAction", m_dragAndDropAction);
    m_settings.setValue("experimentalOpenWithFunction", m_ui->action_enableOpenWithFunction->isChecked());

    QMainWindow::closeEvent(event);
}

void MainWindow::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);

    restoreGeometry(m_settings.value("geometry").toByteArray());
    restoreState(m_settings.value("windowState").toByteArray());

    if(QDir(m_settings.value("leftDirectory").toString()).exists()){
        m_leftWindow->setDirectory(m_settings.value("leftDirectory").toString());
    }
    if(QDir(m_settings.value("rightDirectory").toString()).exists()){
        m_rightWindow->setDirectory(m_settings.value("rightDirectory").toString());
    }

    if(m_settings.value("showHidden") == true){
        m_ui->action_showHiddenFilesDirectories->setChecked(true);
    }

    if(m_settings.value("altRows") == true){
        m_ui->action_alternatingRowColors->setChecked(true);
        toggle_alternatingRowColors();
    }

    if(m_settings.value("darkMode") == true){
        m_ui->action_darkMode->setChecked(true);
        toggle_darkMode();
    }

    if(m_settings.value("previewImage") == true){
        m_ui->action_previewImage->setChecked(true);
        toggle_imagePreview();
    }

    if(m_settings.value("dragAndDropAction") == DragAndDropAction::COPY_ACTION){
        m_dragAndDropAction = DragAndDropAction::COPY_ACTION;
        m_ui->action_setCopyAction->setChecked(true);
        m_ui->action_setMoveAction->setChecked(false);
    }
    else if(m_settings.value("dragAndDropAction") == DragAndDropAction::MOVE_ACTION){
        m_dragAndDropAction = DragAndDropAction::MOVE_ACTION;
        m_ui->action_setCopyAction->setChecked(false);
        m_ui->action_setMoveAction->setChecked(true);
    }

    if(m_settings.value("experimentalOpenWithFunction") == true){
        m_ui->action_enableOpenWithFunction->setChecked(true);
    }
}

void MainWindow::on_pbNewFolder_clicked()
{
    bool ok=true;
    QString text = QInputDialog::getText(this, tr("New folder"),
                                        tr("Directory name:"), QLineEdit::Normal,
                                        "", &ok);

    if (!ok && text.isEmpty()){
        return;
    }
    QString path="";
    if(m_lastClickedWindow){
        path+=m_rightWindow->getDirectory();
    }else{
        path+=m_leftWindow->getDirectory();
    }

    QDir dir(path+"/"+text);
    if(dir.exists()){
        QMessageBox::warning(this,"","Directory with that name already exists!");
        return;
    }else{
        dir.mkpath(path+"/"+text);
    }
}

void MainWindow::on_pbViewClicked()
{
    if(m_viewEnabled){
        m_viewEnabled=false;
        m_preview->getGraphicsView()->setVisible(false);
    }
    else
    {
        m_viewEnabled=true;
        m_preview->getGraphicsView()->setVisible(true);
        m_preview->setScene();
        if(m_leftWindow->getSelectedFilesList()->isEmpty() && m_rightWindow->getSelectedFilesList()->isEmpty()){
            m_preview->getGraphicsView()->setVisible(false);
            QMessageBox::information(this,tr("File Manager"),tr("Please select the items before you click the View button"));
        }

        SelectedFilesList *leftList=m_leftWindow->getSelectedFilesList();
        SelectedFilesList *rightList=m_rightWindow->getSelectedFilesList();

        QChart *chart=m_preview->displayPieChart(leftList,rightList);

        chart->setMaximumSize(m_preview->getGraphicsView()->size());
        chart->setMinimumSize(m_preview->getGraphicsView()->size());
        QChartView *chartView = new QChartView(chart);
        chartView->setMaximumSize(QSize(250,250));
        chartView->setMinimumSize(QSize(220,220));
        m_preview->getGraphicsView()->scene()->addWidget(chartView);
    }
}

void MainWindow::openTerminal()
{
    QString cwd = "";
        if(m_lastClickedWindow){
            cwd+=m_rightWindow->getDirectory();
        }else{
            cwd+=m_leftWindow->getDirectory();
        }
        QByteArray xdg_current_desktop = qgetenv("XDG_CURRENT_DESKTOP");
        QString env = QString::fromLocal8Bit(xdg_current_desktop);
        if (xdg_current_desktop != nullptr)
        {
           if(env.contains("XFCE")){
               QProcess::startDetached("xfce4-terminal", QStringList() << "-e" << "bash -c \"cd " + cwd + "; exec bash\"");
           }else if(env.contains("GNOME")){
               QProcess::startDetached("gnome-terminal", QStringList() << "-e" << "bash -c \"cd " + cwd + "; exec bash\"");
           }else if(env.contains("KDE")){
               QProcess::startDetached("konsole", QStringList() << "-e" << "bash -c \"cd " + cwd + "; exec bash\"");
           }else if(env.contains("LXDE")){
               QProcess::startDetached("lxterminal", QStringList() << "-e" << "bash -c \"cd " + cwd + "; exec bash\"");
           }else if(env.contains("MATE")){
               QProcess::startDetached("mate-terminal", QStringList() << "-e" << "bash -c \"cd " + cwd + "; exec bash\"");
           }else if(env.contains("Cinnamon")){
               QProcess::startDetached("cinnamon-terminal", QStringList() << "-e" << "bash -c \"cd " + cwd + "; exec bash\"");
           }else{
               QProcess::startDetached("terminator", QStringList() << "-e" << "bash -c \"cd " + cwd + "; exec bash\"");
           }
        }
        else
        {
            return;
        }
}

void MainWindow::on_FocusChanged(QWidget * old, QWidget * now){
    Q_UNUSED(old);

    if(now == nullptr){
        return;
    }

    if(now->objectName()=="tvRightWindow"){
        m_lastClickedWindow=true;
    }
    else if(now->objectName()=="tvLeftWindow"){
        m_lastClickedWindow=false;
    }
}

void MainWindow::deleteFiles(bool deleteBetweenCutAndPaste)
{

    SelectedFilesList* list;

    if(!m_isMove || deleteBetweenCutAndPaste){

        if(!m_lastClickedWindow){
            list = m_leftWindow->getSelectedFilesList();
        }else{
            list = m_rightWindow->getSelectedFilesList();
        }
        if(list->empty()){
            QMessageBox::warning(this,"","Nothing is selected!");
            return;
        }

        int n = list->size();
        int ok;
        QString str ="Are you sure you want to move " + (n==1 ? list->first().getName()+" to trash?" : QString::number(n) + " files to trash?");

        ok = QMessageBox::question(this,"",str,QMessageBox::Yes | QMessageBox::No);

        if(ok == QMessageBox::No){
            return;
        }
    }else{
        for(File f : m_filesToDelete){
            QFile file(f.getPath());
            if(file.exists()){
                bool result = file.moveToTrash();
                if(!result){
                    qDebug() <<"Failed to delete: " + file.fileName() + "!";
                }
            }
        }
        return;
    }

    for(File f : *list){
        QFile file(f.getPath());
        if(file.exists()){
            bool result = file.moveToTrash();
            if(!result){
                QMessageBox::warning(this,"","Failed to delete: ",file.fileName(),"!");
            }
        }
    }
    return;
}

void MainWindow::on_actionNew_connection_triggered()
{
    m_ConnectionDialog = new ConnectionDialog(this);
    m_ConnectionDialog->show();
}

void MainWindow::clickedWindow(const QModelIndex &index,Window *window)
{
    m_viewEnabled=false;
    if(window->getSelectedFilesList()->length() == 1 && m_ui->action_previewImage->isChecked())
    {
        File file(window->getModel()->fileInfo(index));
        m_preview->imagePreview(file);
    }
}

Window* MainWindow::getLeftWindow()
{
    return m_leftWindow;
}

Window* MainWindow::getRightWindow()
{
    return m_rightWindow;
}

void MainWindow::clickedTvLeft(const QModelIndex &index)
{
    clickedWindow(index,m_leftWindow);
}

void MainWindow::clickedTvRight(const QModelIndex &index)
{
    clickedWindow(index,m_rightWindow);
}

void MainWindow::copyFiles()
{
    m_isMove=false;

    if(!m_lastClickedWindow){
        m_clipboard->set(m_leftWindow->getSelectedFilesList());
        qDebug() << "Clipboard: " << m_clipboard->to_string();
        m_pathCopiedFrom=m_leftWindow->getDirectory();
    }else{
        m_clipboard->set(m_rightWindow->getSelectedFilesList());
        qDebug() << "Clipboard: " << m_clipboard->to_string();
        m_pathCopiedFrom=m_rightWindow->getDirectory();
    }
    if(m_clipboard->isEmpty()){
        qDebug()<<"Nothing is selected!";
        return;
    }
    return;
}

bool MainWindow::copyDir(const QString sourceDir, const QString destinationDir, const bool overWriteDirectory)
{
    QDir originDirectory(sourceDir);

    if (!originDirectory.exists())
    {
        return false;
    }

    QDir destinationDirectory(destinationDir);

    if(destinationDirectory.exists() && !overWriteDirectory)
    {
        qDebug()<<"Vec postoji direktorijum sa imenom: " + destinationDir.split("/").last();
        return false;
    }
    else if(destinationDirectory.exists() && overWriteDirectory)
    {
        destinationDirectory.removeRecursively();
    }

    originDirectory.mkpath(destinationDir);

    foreach (QString directoryName, originDirectory.entryList(QDir::Dirs | \
                                                              QDir::NoDotAndDotDot))
    {
        QString destinationPath = destinationDir + "/" + directoryName;

        copyDir(sourceDir + "/" + directoryName, destinationPath, overWriteDirectory);
    }

    foreach (QString fileName, originDirectory.entryList(QDir::Files))
    {
        QFile::copy(sourceDir + "/" + fileName, destinationDir + "/" + fileName);
    }


    QDir finalDestination(destinationDir);
    finalDestination.refresh();

    if(finalDestination.exists())
    {
        return true;

    }

    return false;
}

void MainWindow::pasteFiles(){

    bool lastClickedWindow = m_lastClickedWindow;
    QString rightDirectory = m_rightWindow->getDirectory();
    QString leftDirectory = m_leftWindow->getDirectory();
    int override= QMessageBox::Button::No;
    bool isMove = m_isMove;

    if( m_pathCopiedFrom == (lastClickedWindow ? rightDirectory : leftDirectory) ){
        return;
    }

    if(m_clipboard->isEmpty()){
        qDebug()<<"Nothing was copied!";
        return;
    }else{
        for(File f : m_clipboard->toStdList()){
            if(!f.isDir()){
                QFile file(f.getPath());
                if(file.exists()){
                    QFile tmp(lastClickedWindow ? rightDirectory+"/"+f.getName() : leftDirectory+"/"+f.getName());

                    if(tmp.exists()){
                        if(override==QMessageBox::Button::YesToAll){
                            tmp.moveToTrash();
                        }else if(override==QMessageBox::Button::NoToAll){
                            continue;
                        }else{
                            override = QMessageBox::question(this,"","File with name: " + f.getName() + " already exists, do you want to overwrite it?",QMessageBox::Button::No | QMessageBox::Button::NoToAll | QMessageBox::Button::Yes | QMessageBox::Button::YesToAll);
                            if(override==QMessageBox::Button::Yes || override==QMessageBox::Button::YesToAll){
                                tmp.moveToTrash();
                            }else{
                                continue;
                            }
                        }
                    }
                    if(!file.copy( (lastClickedWindow ? rightDirectory+"/"+f.getName() : leftDirectory+"/"+f.getName()))){
                        QMessageBox::warning(this,"Failed to copy!","Failed to copy: " + f.getName());
                    }else if(isMove){
                        m_filesToDelete.append(f);
                    }
                }

            }else if(f.isDir()){
                QDir tmp(lastClickedWindow ? rightDirectory+"/"+f.getName() : leftDirectory+"/"+f.getName());
                if(tmp.exists()){
                    if(override==QMessageBox::Button::YesToAll){
                        tmp.removeRecursively();
                    }else if(override==QMessageBox::Button::NoToAll){
                        continue;
                    }else{
                        override = QMessageBox::question(this,"","File with name: " + f.getName() + " already exists, do you want to overwrite it?",QMessageBox::Button::No | QMessageBox::Button::NoToAll | QMessageBox::Button::Yes | QMessageBox::Button::YesToAll);
                        if(override==QMessageBox::Button::Yes || override==QMessageBox::Button::YesToAll){
                            tmp.removeRecursively();
                        }else{
                            continue;
                        }
                    }
                }
                if(!copyDir(f.getPath(),lastClickedWindow ? rightDirectory+"/"+f.getName() : leftDirectory+"/"+f.getName(),false)){
                    QMessageBox::warning(this,"Failed to copy!","Failed to copy directory: " + f.getName());
                }else if(isMove){
                    m_filesToDelete.append(f);
                }
            }
        }
    }
    if(isMove){
        deleteFiles(false);
        m_isMove=false;
        m_filesToDelete.clear();
    }
}

void MainWindow::moveFiles(){
    copyFiles();
    m_isMove=true;
}

void MainWindow::on_actionHTTP_Client_triggered()
{
    m_HTTP_Client = new HTTP_Client(this);
    m_HTTP_Client->show();
}

