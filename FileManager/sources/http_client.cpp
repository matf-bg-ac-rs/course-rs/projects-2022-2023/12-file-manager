#include "headers/http_client.h"
#include "ui_http_client.h"

#include <QtWidgets>
#include <QtNetwork>
#include <QUrl>

HTTP_Client::HTTP_Client(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HTTP_Client)
{
    ui->setupUi(this);

    connect(ui->leURL, &QLineEdit::textChanged, this, &HTTP_Client::enableDownloadButton);

    QString downloadDirectory = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    ui->leDirectory->setText(QDir::toNativeSeparators(downloadDirectory));
    connect(ui->pbChoseDirectory, &QPushButton::clicked, this, &HTTP_Client::chooseDirectory);

    ui->pbDownload->setDefault(true);
    connect(ui->pbDownload, &QPushButton::clicked, this, &HTTP_Client::downloadFile);
    connect(ui->pbQuit, &QPushButton::clicked, this, &QWidget::close);
}

HTTP_Client::~HTTP_Client()
{
    delete ui;
}


void HTTP_Client::startRequest(const QUrl &requsetedURL)
{
    m_url = requsetedURL;
    m_httpRequestAborted = false;
    m_reply = m_qnam.get(QNetworkRequest(requsetedURL));
    connect(m_reply, &QNetworkReply::finished, this, &HTTP_Client::httpFinished);
    connect(m_reply, &QIODevice::readyRead, this, &HTTP_Client::httpReadyRead);
}

void HTTP_Client::downloadFile()
{
    const QString userUrl = ui->leURL->text().trimmed();
    if (userUrl.isEmpty())
        return;

    const QUrl validUrl = QUrl::fromUserInput(userUrl);
    if (!validUrl.isValid()) {
        QMessageBox::information(this, tr("Error"),
                                 tr("Invalid URL: %1: %2").arg(userUrl, validUrl.errorString()));
        return;
    }

    QString fileName = validUrl.fileName();
    if (fileName.isEmpty())
        fileName = ui->leFile->text().trimmed();
    if (fileName.isEmpty())
        fileName = "index.html";
    QString downloadDirectory = QDir::cleanPath(ui->leDirectory->text().trimmed());
    bool useDirectory = !downloadDirectory.isEmpty() && QFileInfo(downloadDirectory).isDir();
    if (useDirectory)
        fileName.prepend(downloadDirectory + '/');
    if (QFile::exists(fileName)) {
        if (QMessageBox::question(this, tr("Overwrite Existing File"),
                                  tr("There already exists a file called %1%2."
                                     " Overwrite?")
                                     .arg(fileName,
                                          useDirectory
                                           ? QString()
                                           : QStringLiteral(" in the current directory")),
                                     QMessageBox::Yes | QMessageBox::No,
                                     QMessageBox::No)
            == QMessageBox::No) {
            return;
        }
        QFile::remove(fileName);
    }

    m_file = openFileForWrite(fileName);
    if (!m_file)
        return;

    ui->pbDownload->setEnabled(false);

    startRequest(validUrl);
}

std::unique_ptr<QFile> HTTP_Client::openFileForWrite(const QString &fileName)
{
    std::unique_ptr<QFile> file(new QFile(fileName));
    if (!file->open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, tr("Error"),
                                 tr("Unable to save the file %1: %2.")
                                 .arg(QDir::toNativeSeparators(fileName),
                                      file->errorString()));
        return nullptr;
    }
    return file;
}

QNetworkReply *HTTP_Client::getReply() const
{
    return m_reply;
}

void HTTP_Client::cancelDownload()
{
    m_httpRequestAborted = true;
    m_reply->abort();
    ui->pbDownload->setEnabled(true);
}

void HTTP_Client::httpFinished()
{
    QFileInfo fileInfo;
    if (m_file) {
        fileInfo.setFile(m_file->fileName());
        m_file->close();
        m_file.reset();
    }

    if (m_httpRequestAborted) {
        m_reply->deleteLater();
        m_reply = nullptr;
        return;
    }

    if (m_reply->error()) {
        QFile::remove(fileInfo.absoluteFilePath());
        ui->lbMessage->setText(tr("Download failed:\n%1.").arg(m_reply->errorString()));
        ui->pbDownload->setEnabled(true);
        m_reply->deleteLater();
        m_reply = nullptr;
        return;
    }

    const QVariant redirectionTarget = m_reply->attribute(QNetworkRequest::RedirectionTargetAttribute);

    m_reply->deleteLater();
    m_reply = nullptr;

    if (!redirectionTarget.isNull()) {
        const QUrl redirectedUrl = m_url.resolved(redirectionTarget.toUrl());
        if (QMessageBox::question(this, tr("Redirect"),
                                  tr("Redirect to %1 ?").arg(redirectedUrl.toString()),
                                  QMessageBox::Yes | QMessageBox::No) == QMessageBox::No) {
            QFile::remove(fileInfo.absoluteFilePath());
            ui->pbDownload->setEnabled(true);
            ui->lbMessage->setText(tr("Download failed:\nRedirect rejected."));
            return;
        }
        m_file = openFileForWrite(fileInfo.absoluteFilePath());
        if (!m_file) {
            ui->pbDownload->setEnabled(true);
            return;
        }
        startRequest(redirectedUrl);
        return;
    }

    ui->lbMessage->setText(tr("Downloaded %1 bytes to %2\nin\n%3")
                         .arg(fileInfo.size()).arg(fileInfo.fileName(), QDir::toNativeSeparators(fileInfo.absolutePath())));
    ui->pbDownload->setEnabled(true);
}


void HTTP_Client::httpReadyRead()
{
    if (m_file)
        m_file->write(m_reply->readAll());
}

void HTTP_Client::enableDownloadButton()
{
    ui->pbDownload->setEnabled(!ui->leURL->text().isEmpty());
}

void HTTP_Client::chooseDirectory()
{

    QString folder = QFileDialog::getExistingDirectory(this, tr("Open Directory"), qApp->applicationDirPath(), QFileDialog::ShowDirsOnly);
    ui->leDirectory->setText(folder);

}

