#include "headers/contextmenu.h"
#include "headers/propertiesdialog.h"
#include <QDebug>
#include "headers/mainwindow.h"

ContextMenu::ContextMenu(Window *parent, const QPoint &point, SelectedFilesList *list)
    : m_parent(parent)
    , m_position(point)
    , m_files(list)
    , m_actionList(QList<QAction*>())
{
    FilesType filesType = m_files->getFilesType();

    switch(filesType) {
    case FilesType::SINGLE_FILE:
        ContextMenu::addSingleFileActions();
        break;

    case FilesType::MULTIPLE_FILES:
        ContextMenu::addMultipleFilesActions();
        break;

    case FilesType::SINGLE_DIRECTORY:
        ContextMenu::addSingleDirectoryActions();
        break;

    case FilesType::MULTIPLE_DIRECTORIES:
        ContextMenu::addMultipleDirectoriesActions();
        break;

    case FilesType::ZIPPED_FILE:
        ContextMenu::addZippedFileActions();
        break;

    case FilesType::NO_FILE:
        ContextMenu::addNoFileActions();
        break;

    default:
        break;
    }

    this->addActions(m_actionList);
}

ContextMenu::~ContextMenu()
{
    this->disconnect();
    for(auto action : m_actionList)
        delete action;
    this->close();
}

void ContextMenu::execute()
{
    this->exec(m_position);
}

void ContextMenu::addSingleFileActions()
{
    addOpenAction();
    if(openWithFunctionIsEnabled())
        addOpenWithAction();
    insertSeparator();
    addCopyAction();
    addCutAction();
    addPasteAction();
    insertSeparator();
    addCompressAction();
    addCompressToAction();
    insertSeparator();
    addRenameAction();
    insertSeparator();
    addDeleteAction();
    insertSeparator();
    addPropertiesAction();
}

//    // alternativno, moze se jednom naredbom direktno dodati akcija
//    this->addAction(QIcon(":/icons/copy.png"), "Copy",
//              this, &ContextMenu::Action1,
//              QKeySequence(Qt::CTRL + Qt::Key_C));

void ContextMenu::addMultipleFilesActions()
{
    addCopyAction();
    addCutAction();
    addPasteAction();
    insertSeparator();
    addCompressAction();
    addCompressToAction();
    insertSeparator();
    addDeleteAction();
    insertSeparator();
    addPropertiesAction();
}

void ContextMenu::addSingleDirectoryActions()
{
    addOpenAction();
    insertSeparator();
    addCopyAction();
    addCutAction();
    addPasteAction();
    insertSeparator();
    addCompressAction();
    addCompressToAction();
    insertSeparator();
    addRenameAction();
    insertSeparator();
    addDeleteAction();
    insertSeparator();
    addPropertiesAction();
    insertSeparator();
    addOpenTerminalAction();
}

void ContextMenu::addMultipleDirectoriesActions()
{
    addCopyAction();
    addCutAction();
    addPasteAction();
    insertSeparator();
    addCompressAction();
    addCompressToAction();
    insertSeparator();
    addDeleteAction();
    insertSeparator();
    addPropertiesAction();
}

void ContextMenu::addZippedFileActions()
{
    addOpenAction();
    if(openWithFunctionIsEnabled())
        addOpenWithAction();
    insertSeparator();
    addCopyAction();
    addCutAction();
    addPasteAction();
    insertSeparator();
    addCompressAction();
    addCompressToAction();
    addExtractAction();
    addExtractToAction();
    insertSeparator();
    addRenameAction();
    insertSeparator();
    addDeleteAction();
    insertSeparator();
    addPropertiesAction();
}

void ContextMenu::addNoFileActions()
{
    addNewFolderAction();
    insertSeparator();
    addPasteAction();
    insertSeparator();
    addOpenTerminalAction();
}

void ContextMenu::insertSeparator()
{
    QAction *separator = new QAction(this);
    separator->setSeparator(true);
    m_actionList.append(separator);
}

void ContextMenu::addOpenAction()
{
    auto openAction = new QAction("Open", this);
    connect(openAction, &QAction::triggered, this, &ContextMenu::open);
    openAction->setIcon(QIcon(":/icons/open.png"));
    m_actionList.append(openAction);
}

void ContextMenu::addOpenWithAction()
{
    auto openWithAction = new QAction("Open With...", this);
    connect(openWithAction, &QAction::triggered, this, &ContextMenu::openWith);
    m_actionList.append(openWithAction);
}

void ContextMenu::addCopyAction()
{
    auto copyAction = new QAction("Copy", this);
    connect(copyAction, &QAction::triggered, this, &ContextMenu::copyFiles);
    copyAction->setIcon(QIcon(":/icons/copy.png"));
    copyAction->setShortcut(QKeySequence::Copy);
    m_actionList.append(copyAction);
}

void ContextMenu::addCutAction()
{
    auto cutAction = new QAction("Cut", this);
    connect(cutAction, &QAction::triggered, this, &ContextMenu::moveFiles);
    cutAction->setIcon(QIcon(":/icons/cut.png"));
    cutAction->setShortcut(QKeySequence::Cut);
    m_actionList.append(cutAction);
}

void ContextMenu::addPasteAction()
{
    auto pasteAction = new QAction("Paste", this);
    connect(pasteAction, &QAction::triggered, this, &ContextMenu::pasteFiles);
    pasteAction->setIcon(QIcon(":/icons/paste.png"));
    pasteAction->setShortcut(QKeySequence::Paste);
    m_actionList.append(pasteAction);
}

void ContextMenu::addRenameAction()
{
    auto renameAction = new QAction("Rename", this);
    connect(renameAction, &QAction::triggered, this, &ContextMenu::rename);
    renameAction->setIcon(QIcon(":/icons/rename.png"));
    m_actionList.append(renameAction);
}

void ContextMenu::addCompressAction()
{
    auto compressAction = new QAction("Compress", this);
    connect(compressAction, &QAction::triggered, this, &ContextMenu::compress);
    m_actionList.append(compressAction);
}

void ContextMenu::addExtractAction()
{
    auto extractAction = new QAction("Extract", this);
    connect(extractAction, &QAction::triggered, this, &ContextMenu::extract);
    m_actionList.append(extractAction);
}

void ContextMenu::addCompressToAction()
{
    auto compressToAction = new QAction("Compress To...", this);
    connect(compressToAction, &QAction::triggered, this, &ContextMenu::compressTo);
    m_actionList.append(compressToAction);
}

void ContextMenu::addExtractToAction()
{
    auto extractToAction = new QAction("Extract To...", this);
    connect(extractToAction, &QAction::triggered, this, &ContextMenu::extractTo);
    m_actionList.append(extractToAction);
}

void ContextMenu::addDeleteAction()
{
    auto deleteAction = new QAction("Delete", this);
    connect(deleteAction, &QAction::triggered, this, &ContextMenu::deleteFiles);
    deleteAction->setIcon(QIcon(":/icons/delete.png"));
    deleteAction->setShortcut(QKeySequence(Qt::Key_Delete));
    m_actionList.append(deleteAction);
}

void ContextMenu::addPropertiesAction()
{
    auto propertiesAction = new QAction("Properties", this);
    propertiesAction->setIcon(QIcon(":/icons/properties.png"));
    connect(propertiesAction, &QAction::triggered, this, &ContextMenu::properties);
    m_actionList.append(propertiesAction);
}

void ContextMenu::addNewFolderAction()
{
    auto newFolderAction = new QAction("New Folder", this);
    newFolderAction->setIcon(QIcon(":/icons/new_folder.png"));
    connect(newFolderAction, &QAction::triggered, this, &ContextMenu::newFolder);
    newFolderAction->setShortcut(QKeySequence(Qt::Key_F7));
    m_actionList.append(newFolderAction);
}

void ContextMenu::addOpenTerminalAction()
{
    auto openTerminalAction = new QAction("Open In Terminal", this);
    openTerminalAction->setIcon(QIcon(":/icons/open_terminal.png"));
    connect(openTerminalAction, &QAction::triggered, this, &ContextMenu::openTerminal);
    m_actionList.append(openTerminalAction);
}

void ContextMenu::openWith()
{
    File file = m_files->first();
    QUrl fileUrl = QUrl::fromLocalFile(file.getPath());
    QString program = QFileDialog::getOpenFileName(this, tr("Open file with"), fileUrl.toString(), tr("All files (*)"), nullptr, QFileDialog::ReadOnly);

    file.openWith(program);
}

void ContextMenu::open() const
{
    if(m_files->first().isDir()){
        m_parent->setDirectory(m_files->first().getPath());
    }
    else{
        m_files->first().open();
    }
}

void ContextMenu::rename()
{
    File file = m_files->first();

    bool ok;
    QString newName = QInputDialog::getText(this, tr("Rename"),
                                        tr("File name:"), QLineEdit::Normal,
                                        file.getName(),&ok);
    if(!ok){
        return;
    }

    if(newName.isEmpty()){
        QMessageBox::warning(this,"","Error: File name can't be an empty string!");
        return;
    }

    if(file.rename(newName) || file.getName() == newName){
        return;
    }
    else{
        QMessageBox::warning(this,"","Error: File with that name already exists within the directory!");
        return;
    }
}

void ContextMenu::properties() const
{
    m_files->properties();
}

void ContextMenu::newFolder() const
{
    auto mainWindow = (MainWindow *) m_parent->getParent();
    mainWindow->on_pbNewFolder_clicked();
}

void ContextMenu::copyFiles() const
{
    auto mainWindow = (MainWindow *) m_parent->getParent();
    mainWindow->copyFiles();
}

void ContextMenu::pasteFiles() const
{
    auto mainWindow = (MainWindow *) m_parent->getParent();
    mainWindow->pasteFiles();
}

void ContextMenu::deleteFiles() const
{
    auto mainWindow = (MainWindow *) m_parent->getParent();
    mainWindow->deleteFiles(true);
}

void ContextMenu::extract()
{
    SelectedFilesList* selectedFilesList = this->m_files;
    selectedFilesList->prepareExtract(this);
}

void ContextMenu::compress()
{
    SelectedFilesList* selectedFilesList = this->m_files;
    selectedFilesList->prepareCompress(this);
}

void ContextMenu::extractTo()
{
    SelectedFilesList* selectedFilesList = this->m_files;
    selectedFilesList->prepareExtractTo(this);
}

void ContextMenu::compressTo()
{
    SelectedFilesList* selectedFilesList = this->m_files;
    selectedFilesList->prepareCompressTo(this);
}

void ContextMenu::moveFiles()
{
    auto mainWindow = (MainWindow *) m_parent->getParent();
    mainWindow->moveFiles();
}

void ContextMenu::openTerminal() const
{
    auto mainWindow = (MainWindow *) m_parent->getParent();
    mainWindow->openTerminal();

}

bool ContextMenu::openWithFunctionIsEnabled()
{
    auto mainWindow = (MainWindow *) m_parent->getParent();
    return mainWindow->isOpenWithFunctionEnabled();
}
