#include "headers/ftp_manager.h"
#include "ui_ftp_manager.h"
#include <QDebug>


ftp_manager::ftp_manager(QWidget *parent, const QString &ftpAddress, int ftpPort, const QString &username, const QString &password) :
    QWidget(parent),
    ui(new Ui::ftp_manager)
{
    ui->setupUi(this);

    m_manager = new QNetworkAccessManager(this);


    this->m_ftpAddress = "ftp://" +ftpAddress + "/";
    this->m_ftpPort = ftpPort;
    this->m_username = username;
    this->m_password = password;

    getFileList();
}

ftp_manager::~ftp_manager()
{
    delete ui;
}



void ftp_manager::getFileList()
{
    QUrl ftpPath;
    ftpPath.setUrl(m_ftpAddress + "files.txt");
    ftpPath.setUserName(m_username);
    ftpPath.setPassword(m_password);
    ftpPath.setPort(m_ftpPort);

    QNetworkRequest request;
    request.setUrl(ftpPath);

    m_downloadFileListReply = m_manager->get(request);
    connect(m_downloadFileListReply, &QNetworkReply::finished, this, &ftp_manager::downloadFileListFinished);
}

void ftp_manager::downloadFileListFinished()
{
    if(m_downloadFileListReply->error() != QNetworkReply::NoError)
    {
        QMessageBox::warning(this, "Failed", "Failed to load file list: " + m_downloadFileListReply->errorString());
        this->close();
    }
    else
    {
        QByteArray responseData;
        if (m_downloadFileListReply->isReadable())
        {
            responseData = m_downloadFileListReply->readAll();
        }

        ui->lwFileList->clear();
        m_fileList = QString(responseData).split(",");

        if (m_fileList.size() > 0)
        {
            for (int i = 0; i < m_fileList.size(); i++)
            {
                if (m_fileList.at(i) != "")
                {
                    ui->lwFileList->addItem(m_fileList.at(i));
                }
            }
        }
    }
}

void ftp_manager::on_pbOpen_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select File", qApp->applicationDirPath());

    ui->leUploadFileInput->setText(fileName);

}


void ftp_manager::on_pbUpload_clicked()
{
    QFile* file = new QFile(ui->leUploadFileInput->text());
    QFileInfo fileInfo(*file);
    m_uploadFileName = fileInfo.fileName();

    QUrl ftpPath;
    ftpPath.setUrl(m_ftpAddress+m_uploadFileName);
    ftpPath.setUserName(m_username);
    ftpPath.setPassword(m_password);
    ftpPath.setPort(m_ftpPort);

    if(file->open(QIODevice::ReadOnly))
    {
        ui->pbarUpload->setEnabled(true);
        ui->pbarUpload->setValue(0);

        QNetworkRequest request;
        request.setUrl(ftpPath);

        m_uploadFileReply = m_manager->put(request,file);
        connect(m_uploadFileReply, &QNetworkReply::uploadProgress,this,&ftp_manager::uploadFileProgress);
        connect(m_uploadFileReply, &QNetworkReply::finished,this,&ftp_manager::uploadFileFinished);
    }
    else
    {
        QMessageBox::warning(this, "Invalid File", "Failed to open file for upload");
    }
}

void ftp_manager::uploadFileProgress(qint64 bytesSent, qint64 bytesTotal)
{
    qint64 percentage = 100 * bytesSent / bytesTotal;

    ui->pbarUpload->setValue((int) percentage);
}

void ftp_manager::uploadFileFinished()
{
    if(m_uploadFileReply->error() != QNetworkReply::NoError)
    {
        QMessageBox::warning(this, "Failed", "Failed to upload file: " + m_uploadFileReply->errorString());
    }
    else
    {
        bool exists = false;
        if (m_fileList.size() > 0)
        {
            for (int i = 0; i < m_fileList.size(); i++)
            {
                if (m_fileList.at(i) == m_uploadFileName)
                {
                    exists = true;
                }
            }
        }

        if (!exists)
        {
            m_fileList.append(m_uploadFileName);
        }

        QString fileName = "files.txt";
        QFile* file = new QFile(qApp->applicationDirPath() + "/" + fileName);
        file->open(QIODevice::ReadWrite);
        if (m_fileList.size() > 0)
        {
            for (int j = 0; j < m_fileList.size(); j++)
            {
                if (m_fileList.at(j) != "")
                {
                    file->write(QString(m_fileList.at(j) + ",").toUtf8());
                }
            }
        }
        file->close();

        // Re-open the file
        QFile* newFile = new QFile(qApp->applicationDirPath() + "/" + fileName);
        if (newFile->open(QIODevice::ReadOnly))
        {
            // Update file list to server
            QUrl ftpPath;
            ftpPath.setUrl(m_ftpAddress + fileName);
            ftpPath.setUserName(m_username);
            ftpPath.setPassword(m_password);
            ftpPath.setPort(m_ftpPort);

            QNetworkRequest request;
            request.setUrl(ftpPath);
            m_uploadFileListReply = m_manager->put(request, newFile);
            connect(m_uploadFileListReply, &QNetworkReply::finished, this, &ftp_manager::uploadFileListFinished);
            file->close();
        }

        QMessageBox::information(this, "Success", "File successfully uploaded.");
    }
}


void ftp_manager::uploadFileListFinished()
{
    if(m_uploadFileListReply->error() != QNetworkReply::NoError)
    {
        QMessageBox::warning(this, "Failed", "Failed to update file list: " + m_uploadFileListReply->errorString());
    }
    else
    {
        getFileList();
    }
}



void ftp_manager::on_pbSetFolder_clicked()
{
    QString folder = QFileDialog::getExistingDirectory(this, tr("Open Directory"), qApp->applicationDirPath(), QFileDialog::ShowDirsOnly);

    ui->leDownloadPath->setText(folder);
}


void ftp_manager::on_lwFileList_itemDoubleClicked(QListWidgetItem *item)
{
    m_downloadFileName = item->text();

    // Check folder
    QString folder = ui->leDownloadPath->text();
    if (folder != "" && QDir(folder).exists())
    {
        QUrl ftpPath;
        ftpPath.setUrl(m_ftpAddress + m_downloadFileName);
        ftpPath.setUserName(m_username);
        ftpPath.setPassword(m_password);
        ftpPath.setPort(m_ftpPort);

        QNetworkRequest request;
        request.setUrl(ftpPath);

        m_downloadFileReply = m_manager->get(request);
        connect(m_downloadFileReply, &QNetworkReply::downloadProgress, this, &ftp_manager::downloadFileProgress);
        connect(m_downloadFileReply, &QNetworkReply::finished, this, &ftp_manager::downloadFileFinished);
    }
    else
    {
        QMessageBox::warning(this, "Invalid Path", "Please set the download path before download.");
    }
}



void ftp_manager::downloadFileProgress(qint64 byteReceived,qint64 bytesTotal)
{
    qint64 percentage = 100 * byteReceived / bytesTotal;

    ui->pbarDowload->setValue((int) percentage);
}

void ftp_manager::downloadFileFinished()
{
    if(m_downloadFileReply->error() != QNetworkReply::NoError)
    {
        QMessageBox::warning(this, "Failed", "Failed to download file: " + m_downloadFileReply->errorString());
    }
    else
    {
        QByteArray responseData;
        if (m_downloadFileReply->isReadable())
        {
            responseData = m_downloadFileReply->readAll();
        }

        if (!responseData.isEmpty())
        {
            // Download finished
            QString folder = ui->leDownloadPath->text();
            QFile file(folder + "/" + m_downloadFileName);
            file.open(QIODevice::WriteOnly);
            file.write((responseData));
            file.close();

            QMessageBox::information(this, "Success", "File successfully downloaded.");
        }
    }
}

QNetworkReply *ftp_manager::downloadFileListReply() const
{
    return m_downloadFileListReply;
}


