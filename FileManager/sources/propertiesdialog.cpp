#include "headers/propertiesdialog.h"
#include "ui_propertiesdialog.h"
#include "headers/preview.h"

#include <QMessageBox>

PropertiesDialog::PropertiesDialog(QWidget *parent, const SelectedFilesList *selectedFilesList) :
    QDialog(parent),
    ui(new Ui::PropertiesDialog)
{
    m_selectedFilesList = selectedFilesList;
    ui->setupUi(this);

    if(m_selectedFilesList->size()==1){
        File file = m_selectedFilesList->first();

        // INFO
        if(file.isDir()){
            ui->leName->setText(file.getName());
            int position = selectedFilesList->first().getPath().lastIndexOf('/');
            QString dirPath = selectedFilesList->first().getPath().left(position);
            ui->lLocationInfo->setText(dirPath);
            ui->lTypeInfo->setText("File folder");
            ui->lSizeInfo->setText(formatSize(file.getSize()));
            ui->lCreatedInfo->setText(file.getCreated().toString());
            ui->lModifiedInfo->setText(file.getModified().toString());
            ui->lAccessedInfo->setText(file.getOpened().toString());
            QPair<qint64,qint64> contents = file.getContents();
            ui->lContainsInfo->setText(QString::number(contents.first)+" Files, " + QString::number(contents.second-1) + " Folders");
        }
        else{
            ui->leName->setText(file.getName());
            int position = selectedFilesList->first().getPath().lastIndexOf('/');
            QString dirPath = selectedFilesList->first().getPath().left(position);
            ui->lLocationInfo->setText(dirPath);
            if(file.getExtension()!=""){
                ui->lTypeInfo->setText(QString(".").append(file.getExtension().append(" file")));
            }
            else{
                ui->lTypeInfo->setText("Unknown file");
            }
            ui->lSizeInfo->setText(formatSize(file.getSize()));
            ui->lContains->hide();
            ui->lContainsInfo->hide();
            ui->lCreatedInfo->setText(file.getCreated().toString());
            ui->lModifiedInfo->setText(file.getModified().toString());
            ui->lAccessedInfo->setText(file.getOpened().toString());
        }

        //PERMISSIONS
        QFileDevice::Permissions permissions = file.getPermissions();

        ui->cbReadUser->setChecked(permissions & QFileDevice::ReadUser);
        ui->cbWriteUser->setChecked(permissions & QFileDevice::WriteUser);
        ui->cbExecuteUser->setChecked(permissions & QFileDevice::ExeUser);

        ui->cbReadGroup->setChecked(permissions & QFileDevice::ReadGroup);
        ui->cbWriteGroup->setChecked(permissions & QFileDevice::WriteGroup);
        ui->cbExecuteGroup->setChecked(permissions & QFileDevice::ExeGroup);

        ui->cbReadOther->setChecked(permissions & QFileDevice::ReadOther);
        ui->cbWriteOther->setChecked(permissions & QFileDevice::WriteOther);
        ui->cbExecuteOther->setChecked(permissions & QFileDevice::ExeOther);
    }
    else{
        ui->twCentralTab->removeTab(1);
        ui->lName->hide();
        ui->leName->hide();
        ui->lCreated->hide();
        ui->lCreatedInfo->hide();
        ui->lModified->hide();
        ui->lModifiedInfo->hide();
        ui->lAccessed->hide();
        ui->lAccessedInfo->hide();
        ui->bottomLine->hide();
        ui->topLine->hide();

        int position = selectedFilesList->first().getPath().lastIndexOf('/');
        QString dirPath = selectedFilesList->first().getPath().left(position);
        ui->lLocationInfo->setText(dirPath);
        ui->lTypeInfo->setText("Multiple files");
        qint64 size = 0;
        qint64 fileCount = 0;
        qint64 dirCount = 0;
        for(File f : *selectedFilesList){
            size+= f.getSize();
            QPair<qint64,qint64> contents = f.getContents();
            fileCount += contents.first;
            dirCount += contents.second;
        }
        ui->lSizeInfo->setText(formatSize(size));
        ui->lContainsInfo->setText(QString::number(fileCount)+" Files, " + QString::number(dirCount) + " Folders");
    }
    displayPieChart();
}

PropertiesDialog::~PropertiesDialog()
{
    delete ui;
}

void PropertiesDialog::on_pbCancel_clicked()
{
    this->close();
}

void PropertiesDialog::on_pbOk_clicked()
{
    if(m_selectedFilesList->size()==1){
        File file = m_selectedFilesList->first();
        QString newName = ui->leName->text();
        if(newName==""){
            QMessageBox::warning(this,"","Name can't be an empty string!");
            ui->leName->setText(file.getName());
            return;
        }
        else if(!file.rename(newName) && file.getName()!=newName){
            QMessageBox::warning(this,"","File with that name already exists!");
            ui->leName->setText(file.getName());
            return;
        }
    }
    this->close();
    return;
}

QString PropertiesDialog::formatSize(qint64 size) const{
    QStringList units = {"Bytes", "KB", "MB", "GB", "TB", "PB"};
    int i;
    double outputSize = size;
    for(i=0; i<units.size()-1; i++) {
        if(outputSize<1024) break;
        outputSize= outputSize/1024;
    }
    return QString("%0 %1").arg(outputSize, 0, 'f', 2).arg(units[i]);
}

void PropertiesDialog::displayPieChart()
{
    auto size = ui->gvChart->size();
    Preview *preview = new Preview(ui->gvChart);
    preview->getGraphicsView()->setVisible(true);
    preview->getGraphicsView()->setMaximumSize(size);
    QChart *chart= preview->displayPieChart(m_selectedFilesList, new SelectedFilesList());
    QChartView *chartView = new QChartView(chart);
    chartView->setMinimumSize(size.width()-10, size.height()-10);
    preview->setScene();
    preview->getGraphicsView()->scene()->addWidget(chartView);
}

QString PropertiesDialog::getType()
{
    return ui->lTypeInfo->text();
}

void PropertiesDialog::on_btApply_clicked()
{
    File file = m_selectedFilesList->first();
    QFileDevice::Permissions newPermissions;

    newPermissions.setFlag(QFileDevice::ReadUser, ui->cbReadUser->isChecked());
    newPermissions.setFlag(QFileDevice::WriteUser, ui->cbWriteUser->isChecked());
    newPermissions.setFlag(QFileDevice::ExeUser, ui->cbExecuteUser->isChecked());

    newPermissions.setFlag(QFileDevice::ReadGroup, ui->cbReadGroup->isChecked());
    newPermissions.setFlag(QFileDevice::WriteGroup, ui->cbWriteGroup->isChecked());
    newPermissions.setFlag(QFileDevice::ExeGroup, ui->cbExecuteGroup->isChecked());

    newPermissions.setFlag(QFileDevice::ReadOther, ui->cbReadOther->isChecked());
    newPermissions.setFlag(QFileDevice::WriteOther, ui->cbWriteOther->isChecked());
    newPermissions.setFlag(QFileDevice::ExeOther, ui->cbExecuteOther->isChecked());

    if(!file.setPermissions(newPermissions)){
        QMessageBox::warning(this,"","Error changing permissions!");
        return;
    }
}

