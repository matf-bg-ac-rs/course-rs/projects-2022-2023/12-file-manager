#include "headers/file.h"

#include <QDebug>
#include <QDesktopServices>

const QList<QString> File::zipExtensions = {"zip", "jar"};  // izbrisane ostale ekstenzije :(
const QList<QString> File::imgExtensions = {"png", "jpg", "jpeg", "gif", "svg"};

File::File(const QString name, const QString path, const qint64 size,
           const QString extension,
           const QDateTime created, const QDateTime modified,
           const QFileDevice::Permissions permissions,
           const bool isDir,
           const QDateTime opened)
    : m_name(name)
    , m_path(path)
    , m_size(size)
    , m_extension(extension)
    , m_created(created)
    , m_modified(modified)
    , m_permissions(permissions)
    , m_isDir(isDir)
    , m_opened(opened)
{

}

File::File(const QFileInfo &fileInfo)
    : m_name(fileInfo.fileName())
    , m_path(fileInfo.absoluteFilePath())
    , m_size(fileInfo.size())
    , m_extension(fileInfo.suffix())
    , m_created(fileInfo.created())
    , m_modified(fileInfo.lastModified())
    , m_permissions(fileInfo.permissions())
    , m_isDir(fileInfo.isDir())
    , m_opened(fileInfo.lastRead())
{

}

QString File::getName() const { return m_name; }

bool File::isZip() const { return File::zipExtensions.contains(this->m_extension); }

bool File::isImg() const { return File::imgExtensions.contains(this->m_extension); }

void File::open()
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(m_path));
}

void File::openWith(const QString &program)
{
    if (!program.isEmpty()) {
        QStringList arguments;
        arguments << m_path;
        QProcess::startDetached(program, arguments);
    }
}

bool File::operator==(const File &f) const
{
    return this->getName() == f.getName();

}

QString File::getPath() const {
    return m_path;
}

bool File::rename(const QString &newName)
{
    if(newName.isEmpty())
        return false;

    QFile file(m_path);
    int position = m_path.lastIndexOf('/');
    QString newPath = m_path.left(position).append("/").append(newName);
    if(file.rename(m_path, newPath)){
        m_path = newPath;
        m_name = newPath.split('/').last();
        return true;
    }
    else{
        return false;
    }
}

bool File::setPermissions(const QFileDevice::Permissions permissions)
{
    QFile file(m_path);
    return file.setPermissions(permissions);
}

QString File::getExtension() const
{
    return m_extension;
}

bool File::isDir() const
{
    return m_isDir;
}

qint64 File::getSize() const
{
    if(this->isDir()){
        qint64 size = 0;
        QDir dir(m_path);
        QDir::Filters fileFilters = QDir::Files|QDir::Dirs|QDir::NoDotAndDotDot|QDir::System|QDir::Hidden;
        for(QString filePath : dir.entryList(fileFilters)) {
            File f(QFileInfo(dir,filePath));
            size+= f.getSize();
        }
        return size;
    }
    else{
        return m_size;
    }
}

QDateTime File::getCreated() const
{
    return m_created;
}

QDateTime File::getModified() const
{
    return m_modified;
}

QFileDevice::Permissions File::getPermissions() const
{
    return m_permissions;
}

QDateTime File::getOpened() const
{
    return m_opened;
}

QPair<qint64,qint64> File::getContents() const
{
    if(this->isDir()){
        QPair<qint64,qint64> contents(0,1);
        QDir dir(m_path);
        QDir::Filters fileFilters = QDir::Files|QDir::Dirs|QDir::NoDotAndDotDot|QDir::System|QDir::Hidden;
        for(QString filePath : dir.entryList(fileFilters)) {
            File f(QFileInfo(dir,filePath));
            contents.first+=f.getContents().first;
            contents.second+=f.getContents().second;
        }
        return contents;
    }
    else{
        return QPair<qint64,qint64>(1,0);
    }
}
