#include "headers/window.h"
#include "headers/file.h"
#include "headers/contextmenu.h"
#include "headers/mainwindow.h"

Window::Window(QWidget *parent, FileSystemModel *fileSystemModel, QTableView *tableView, QLineEdit *lePath)
    : m_parent(parent)
    , m_model(fileSystemModel)
    , m_view(tableView)
    , m_lePath(lePath)
{
    m_directory = QDir::homePath();
    m_lePath->setText(m_directory);

    m_view->setModel(m_model);
    m_view->setRootIndex(m_model->setRootPath(m_directory));

    m_view->defaultDropAction();

    m_selectedFilesList = new SelectedFilesList();

    m_view->horizontalHeader()->setResizeContentsPrecision(-1);
    m_view->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    m_view->horizontalHeader()->resizeSection(DisplayedContentsColumn::NAME, 400);

    m_filters = QDir::AllDirs | QDir::Files | QDir::NoDot;
    m_model->setFilter(m_filters);

    connect(m_view, &QAbstractItemView::activated,
            this, &Window::on_activated);

    connect(m_view->selectionModel(), &QItemSelectionModel::selectionChanged,
            this, &Window::on_selectionChanged);

    connect(m_lePath, &QLineEdit::editingFinished,
            this, &Window::search);

    connect(m_view, &QAbstractItemView::pressed,
            this, &Window::on_mousePressed);

    connect(m_view, &QWidget::customContextMenuRequested,
            this, &Window::showContextMenu);
}


inline QString Window::getDirectory() const { return m_directory; }

void Window::setDirectory(const QString directory)
{
    if(directory == "/.."){
        return;
    }
    m_directory = directory;
    m_view->setRootIndex(m_model->setRootPath(directory));
    m_lePath->setText(directory);
}

void Window::showContextMenu(const QPoint& pos)
{
    if(QGuiApplication::keyboardModifiers() != Qt::NoModifier)
        return;

    qDebug() << "Context menu requested";

    QPoint globalPos = m_view->mapToGlobal(pos);
    int offsetY = m_view->horizontalHeader()->height();
    globalPos += QPoint(0, offsetY);

    ContextMenu rightClickMenu(this, globalPos, m_selectedFilesList);
    rightClickMenu.execute();
}

void Window::on_mousePressed(const QModelIndex &index)
{
    if(QApplication::mouseButtons().testFlag(Qt::MouseButton::LeftButton))
    {
        qDebug() << "Left click on selected: " << m_selectedFilesList->to_string();

        // ovde moze da se obradjuje left-click
    }

    else if(QApplication::mouseButtons().testFlag(Qt::MouseButton::RightButton))
    {
        qDebug() << "Right click on selected: " << m_selectedFilesList->to_string();

        // ovde moze da se obradjuje right-click

    }

    else
    {
        m_view->selectionModel()->clear();
    }
}

void Window::on_selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    for(auto i : selected.indexes()) {
        if(i.column() == DisplayedContentsColumn::NAME)
        {
            File file(m_model->fileInfo(i));
            if(file.getName() != "..")
                m_selectedFilesList->append(file);
        }
    }
    for(auto i : deselected.indexes()) {
        if(i.column() == DisplayedContentsColumn::NAME)
        {
            File *file = new File(m_model->fileInfo(i));
            m_selectedFilesList->removeOne(*file);
            delete file;
        }
    }
}

void Window::on_activated(const QModelIndex &index)
{
    // ne dozvoljava da se registruje double-click ako se drzi CTRL i/ili SHIFT
    // ili ako se uradi dupli klik sa dugmetom koje nije levi klik
    if(QGuiApplication::keyboardModifiers() != Qt::NoModifier ||
       !QApplication::mouseButtons().testFlag(Qt::MouseButton::LeftButton))
    {
        return;
    }

    if(m_model->fileInfo(index).isDir()){
        setDirectory(m_model->fileInfo(index).absoluteFilePath());
    }
    else{
        File file(m_model->fileInfo(index));
        file.open();
    }
}

void Window::toggle_ShowHiddenFilesAndDirectories()
{
    m_filters ^= QDir::Hidden;
    m_model->setFilter(m_filters);
}

void Window::toggle_alternatingRowColors(const bool toggle)
{
    m_view->setAlternatingRowColors(toggle);
}

void Window::search()
{
    QString newPath = m_lePath->text();
    if(QDir(newPath).exists()){
        setDirectory(newPath);
    }
    else if(QFile::exists(newPath)){
        int position = newPath.lastIndexOf('/');
        QString newDir = newPath.left(position);
        setDirectory(newDir);
        QString newFile = newPath.section('/',-1);
        File file(QFileInfo(newDir, newFile));
        file.open();
    }
    else{
        m_lePath->setText(getDirectory());
    }
}

SelectedFilesList* Window::getSelectedFilesList(){ return m_selectedFilesList; }

FileSystemModel* Window::getModel(){
    return m_model;
}

QWidget* Window::getParent()
{
    return m_parent;
}

Clipboard *Window::getClipboard()
{
    auto mainWindow = (MainWindow *) getParent();
    return mainWindow->getClipboard();
}
