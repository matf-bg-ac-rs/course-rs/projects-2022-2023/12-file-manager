#include "headers/connectiondialog.h"
#include "ui_connectiondialog.h"
#include "headers/ftp_manager.h"

ConnectionDialog::ConnectionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConnectionDialog)
{
    ui->setupUi(this);
}

ConnectionDialog::~ConnectionDialog()
{
    delete ui;
}


void ConnectionDialog::on_pbConnection_clicked()
{
    QString ftpAddress = ui->leAdress->text();
    int ftpPort = ui->sbPort->value();
    QString username = ui->leUsername->text();
    QString password = ui->lePassword->text();
    m_ftpManager = new ftp_manager(nullptr, ftpAddress,ftpPort,username,password);
    m_ftpManager->show();
    this->close();
}

