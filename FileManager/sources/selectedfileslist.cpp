#include "headers/selectedfileslist.h"
#include "headers/propertiesdialog.h"
#include "headers/contextmenu.h"

#include <quazip.h>
#include "JlCompress.h"
#include <QDebug>
#include <QMessageBox>
#include <QInputDialog>

QString SelectedFilesList::to_string() const
{
    if(this->isEmpty())
        return "nothing";

    QString str("[");
    for(auto i : this->toStdList())
    {
        str.append(i.getName());
        str.append(", ");
    }
    str.remove(str.lastIndexOf(","), 2);
    str.append("]");
    return str;
}


FilesType SelectedFilesList::getFilesType() const
{
    if(this->size() == 0) {
        return FilesType::NO_FILE;
    }

    if(this->size() == 1) {
        if(this->first().isDir())
            return FilesType::SINGLE_DIRECTORY;
        if(this->first().isZip())
            return FilesType::ZIPPED_FILE;
        return FilesType::SINGLE_FILE;
    }

    for(auto file : this->toStdList())
        if(!file.isDir())
            return FilesType::MULTIPLE_FILES;

    return FilesType::MULTIPLE_DIRECTORIES;
}

void SelectedFilesList::properties() const
{
    PropertiesDialog *propertiesDialog = new PropertiesDialog(nullptr, this);
    propertiesDialog->show();
}

std::list<File> SelectedFilesList::toStdList() const
{
    std::list<File> stdlist(this->begin(), this->end());
    return stdlist;
}


void SelectedFilesList::prepareExtract(QMenu *contextMenu) const
{
    if(!this->isEmpty()) {
        auto answer = QMessageBox::question(contextMenu,"Extract","Do you want to extract an archive?");

        if(answer == QMessageBox::Yes) {
            this->extract();
        }
    }
    else {
        qDebug() << "Select file to extract";
        return;
    }
}

void SelectedFilesList::prepareCompress(QMenu *contextMenu) const
{

    if(!this->isEmpty()) {
        bool ok;
        QString newName = QInputDialog::getText(contextMenu, "Compress file", "Enter file name (.zip) :", QLineEdit::Normal, "", &ok);

        if(!ok && newName.isEmpty())
            return;
        this->compress(newName);
    }
    else {
        qDebug() << "Select files to compress";
        return;
    }
}

void SelectedFilesList::prepareExtractTo(QMenu *contextMenu) const
{
    if(!this->isEmpty()) {
        QString rootDir = QFileDialog::getExistingDirectory(contextMenu, "Choose a directory to extract to:",
                                                     "/home",
                                                     QFileDialog::ShowDirsOnly
                                                     | QFileDialog::DontResolveSymlinks);
        if(rootDir != ""){
            this->extract(rootDir);
        }
    }
    else {
        qDebug() << "Select file to extract";
        return;
    }
}

void SelectedFilesList::prepareCompressTo(QMenu *contextMenu) const
{
    if(!this->isEmpty()) {
        bool ok;
        QString newName = QInputDialog::getText(contextMenu, "Compress file into a directory", "Enter file name (.zip) and choose directory:", QLineEdit::Normal, "", &ok);

        if(!ok && newName.isEmpty())
            return;
        QString rootDir = QFileDialog::getExistingDirectory(contextMenu, "Open Directory",
                                                     "/home",
                                                     QFileDialog::ShowDirsOnly
                                                     | QFileDialog::DontResolveSymlinks);
        this->compress(newName, rootDir);
    }
    else {
        qDebug() << "Select files to compress";
        return;
    }
}


void SelectedFilesList::compress(QString newFileName, QString path)  const{
    newFileName += ".zip";

    File first = this->first();
    if(path == ""){
        path = first.getPath().left(first.getPath().lastIndexOf('/'));
    }
    path +=  + '/' + newFileName;

    QuaZip zip(newFileName);

 //   QString dirPath = first.getPath().left(first.getPath().lastIndexOf('/'));
    bool flag = archive(path, *this);
    if(!flag) {
        qDebug() << "Greska";
    }
    else{
        qDebug() << "Ok";
    }
}


void SelectedFilesList::extract(QString path) const {
    File file = this->first();

    // ako nije prosledjen path, extract radi u folderu selektovanih fajlova
    if(path == ""){
        path = file.getPath().left(file.getPath().lastIndexOf('/'));
    }

    QDir dir(path);

    /// ako se ime arhive ne zavrsava na .zip, folder u koji se otpakuje se zove {imeArhive}Archive
    QString newDirName;
    if(file.getName().lastIndexOf('.') != -1){
        newDirName = file.getName().left(file.getName().lastIndexOf('.'));
    }
    else {
        newDirName = file.getName() + "Archive";
    }
    if (!dir.mkpath(dir.absoluteFilePath(newDirName))) {
        qDebug() << "Greska pri pravljenju dir za unzip";
    }
    //                                        zip path     gde da otpakuje
    QStringList list = JlCompress::extractDir(file.getPath(), dir.absoluteFilePath(newDirName));
    if(list.isEmpty()) {
        qDebug() << "Greska extractDir";
        // ukloni novokreirani dir
        dir.rmdir(newDirName);
    }
}


// Rekurzivno prolazi kroz dir i dodaje putanje fajlova u list
void SelectedFilesList::recurseAddDir(QDir d, QStringList & list) {
    QStringList qsl = d.entryList(QDir::NoDotAndDotDot | QDir::Dirs | QDir::Files);
    foreach (QString file, qsl) {
        QFileInfo finfo(QString("%1/%2").arg(d.path()).arg(file));
        if (finfo.isSymLink())
            continue; // ovde bio return
        if (finfo.isDir()) {
    //        QString dirname = finfo.fileName();
            QDir sd(finfo.filePath());
            recurseAddDir(sd, list);
        } else
            list << QDir::toNativeSeparators(finfo.filePath());
    }
}


// moja verzija fje koja prima listu FILE objekata
bool SelectedFilesList::archive(const QString & filePathOfZip, QList<File> listOfSelectedFiles) {
    // pravi zip fajl na putanji filePathOfZip
    QuaZip zip(filePathOfZip);
    zip.setFileNameCodec("IBM866");

    // Otvara fajl
    if (!zip.open(QuaZip::mdCreate)) {
        // myMessageOutput(true, QtDebugMsg, QString("testCreate(): zip.open(): %1").arg(zip.getZipError()));
        return false;
    }

    // Predpostavljam da su svi selektovani fajlovi i folderi u istom korenom direktorijumu
    File first = listOfSelectedFiles.first();
    QString dirPath = first.getPath().left(first.getPath().lastIndexOf('/'));
    QDir rootDir(dirPath);


    // fileNameList sadrzi putanje svih selektovanih fajlova kao i fajlova u selektovanim direktorijumima
    QStringList fileNameList;
    foreach(File file, listOfSelectedFiles){
        if(!file.isDir()) {
            fileNameList << QDir::toNativeSeparators(file.getPath());
        }
        else {
            recurseAddDir(QDir(file.getPath()), fileNameList);
        }
    }

    //
    // Pravi listu FileInfo od putanja fajlova iz fileNameList
    QFileInfoList files;
    foreach (QString fn, fileNameList) files << QFileInfo(fn);

    QFile inFile;
    QuaZipFile outFile(&zip);

    char c;
    foreach(QFileInfo fileInfo, files) {

        if (!fileInfo.isFile())
            continue;

        // Relativne putanje od korenog direktorijuma, gde se vide poddirektorijumi
        QString fileNameWithRelativePath = fileInfo.filePath().remove(0, rootDir.absolutePath().length() + 1);

        inFile.setFileName(fileInfo.filePath());

        if (!inFile.open(QIODevice::ReadOnly)) {
            qDebug() << "Error opening inFile";
            return false;
        }

        if (!outFile.open(QIODevice::WriteOnly, QuaZipNewInfo(fileNameWithRelativePath, fileInfo.filePath()))) {
            qDebug() << "Error openign outFile";
            return false;
        }

        // prepisuje iz inFile u outFile
        while (inFile.getChar(&c) && outFile.putChar(c));

        if (outFile.getZipError() != UNZ_OK) {
            qDebug() << "Error copy contents";
            return false;
        }

        outFile.close();

        if (outFile.getZipError() != UNZ_OK) {
            qDebug() << "Error outFile.close()";
            return false;
        }

        inFile.close();
    }


    zip.close();

    if (zip.getZipError() != 0) {
        qDebug() << "Error zip.close()";
        return false;
    }

    return true;


}
