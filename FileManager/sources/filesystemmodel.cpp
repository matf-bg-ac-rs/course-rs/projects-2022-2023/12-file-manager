#include "headers/filesystemmodel.h"
#include "headers/mainwindow.h"

#include <QDebug>

FileSystemModel::FileSystemModel(QObject *parent, DragAndDropAction *action)
    : m_parent(parent)
    , m_action(action)
{

}

Qt::ItemFlags FileSystemModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags defaultFlags = QFileSystemModel::flags(index);

    if (!index.isValid())
        return defaultFlags;

    const QFileInfo& fileInfo = this->fileInfo(index);

    if(fileInfo.fileName() == "..")
    {
        return Qt::ItemIsEnabled;
    }

    // The target
    if (fileInfo.isDir())
    {
        // allowed drop
        return Qt::ItemIsDropEnabled | defaultFlags;
    }
    // The source: should be directory (in that case)
    else if (fileInfo.isFile())
    {
        // allowed drag
        return Qt::ItemIsDragEnabled | defaultFlags;
    }

    return defaultFlags;
}

bool FileSystemModel::canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(action);

    QFileInfo target;
    if (row < 0 && column < 0)
        target = this->fileInfo(parent);
    else
        target = this->fileInfo(this->index(row, column, parent));

    const QString targetPath = target.filePath();
    const QString source = data->text();
    bool TCSoTICD = targetContainsSource_or_targetIsCurrentDirectory(source, targetPath);

    return target.isDir() && target.isWritable() && (target.fileName() != "..")
            && !TCSoTICD;
}

bool FileSystemModel::targetContainsSource_or_targetIsCurrentDirectory(const QString source, QString targetPath) const
{
    for(QString url : source.split("\n"))
    {
        QString sourcePath = QUrl(url).toLocalFile();
        if(sourcePath.isEmpty()) // last url is an empty string, because there is a '\n' at the end of source
            continue;

        if(sourcePath == targetPath || sourcePath.section('/', 0, -2) == targetPath.remove(0, 1))
            return true;
    }
    return false;
}

bool FileSystemModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    Q_UNUSED(action);

    QDir targetDir;
    if (row < 0 && column < 0)
        targetDir = QDir(this->fileInfo(parent).absoluteFilePath());
    else
        targetDir = QDir(this->fileInfo(this->index(row, column, parent)).absoluteFilePath());

    QMap<QFile*, QString> dataMap;
    QMap<QFile*, bool> removeMap;

    int overwrite = QMessageBox::Button::No;

    for(QString url : data->text().split("\n"))
    {
        QString path = QUrl(url).toLocalFile();
        if(path.isEmpty()) // last url is an empty string, because there is a '\n' at the end of data->text()
            continue;

        QFile *fileObject = new QFile(path);
        if(!(fileObject->permissions() & QFile::WriteUser))
        {
            QMessageBox::warning((QWidget*)m_parent, "Warning", "Access denied - no permission to write!");
            return false;
        }

        auto targetPath = targetDir.absoluteFilePath(QFileInfo(path).fileName());

        if(targetDir.exists(targetPath))
        {
            if(overwrite == QMessageBox::Button::NoToAll)
                removeMap.insert(fileObject, false);
            else if(overwrite == QMessageBox::Button::YesToAll)
                removeMap.insert(fileObject, true);
            else
            {
                overwrite = QMessageBox::question((QWidget*)m_parent, "Overwrite",
                                                      "File " + QFileInfo(path).fileName() + " already exists, do you want to overwrite it?",
                                                      QMessageBox::Button::No | QMessageBox::Button::NoToAll | QMessageBox::Button::Yes | QMessageBox::Button::YesToAll);

                if(overwrite == QMessageBox::Button::Yes || overwrite == QMessageBox::Button::YesToAll)
                    removeMap.insert(fileObject, true);
                else
                    removeMap.insert(fileObject, false);
            }
        }

        dataMap.insert(fileObject, targetPath);
    }

    for(auto it = dataMap.begin(); it != dataMap.end(); it++)
    {
        if(!removeMap.contains(it.key()))
        {
            if(*m_action == DragAndDropAction::MOVE_ACTION)
                moveFile(it.key(), it.value());

            else if(*m_action == DragAndDropAction::COPY_ACTION)
                copyFile(it.key(), it.value());

        }
        else if(removeMap.value(it.key()) == true)
        {
            if(!removeFile(it.value()))
                return false;

            if(*m_action == DragAndDropAction::MOVE_ACTION)
                moveFile(it.key(), it.value());

            else if(*m_action == DragAndDropAction::COPY_ACTION)
                copyFile(it.key(), it.value());
        }
    }
    return true;
}

bool FileSystemModel::moveFile(QFile *file, const QString targetPath)
{
    if(!file->rename(targetPath))
    {
        QMessageBox::warning((QWidget*)m_parent, "Error", "Failed to move file " + file->fileName());
        return false;
    }
    return true;
}

bool FileSystemModel::copyFile(QFile *file, const QString targetPath)
{
    auto fi = QFileInfo(*file);
    if(fi.isDir())
    {
        if(!MainWindow::copyDir(fi.absoluteFilePath(), targetPath, false))
        {
            QMessageBox::warning((QWidget*)m_parent, "Error", "Failed to copy directory " + fi.absoluteFilePath());
            return false;
        }
    }
    else if(!QFile::copy(file->fileName(), targetPath))
    {
        QMessageBox::warning((QWidget*)m_parent, "Error", "Failed to copy file " + file->fileName());
        return false;
    }
    return true;
}

bool FileSystemModel::removeFile(const QString path)
{
    if(!QFile::moveToTrash(path))
    {
        QMessageBox::warning((QWidget*)m_parent, "Error", "Overwrite: failed to remove file " + path);
        return false;
    }
    return true;
}
