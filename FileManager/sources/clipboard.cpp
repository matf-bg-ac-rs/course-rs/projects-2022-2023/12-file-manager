#include "headers/clipboard.h"

Clipboard::Clipboard()
    : m_list(new SelectedFilesList())
{

}

void Clipboard::clear() {
    delete m_list;
    m_list = nullptr;
}

void Clipboard::set(const SelectedFilesList *list)
{
    m_list->clear();
    m_list = new SelectedFilesList(*list);
}

bool Clipboard::isEmpty() const
{
    return m_list->isEmpty();
}

QString Clipboard::to_string() const
{
    return m_list->to_string();
}

std::list<File> Clipboard::toStdList() const
{
    return m_list->toStdList();
}
