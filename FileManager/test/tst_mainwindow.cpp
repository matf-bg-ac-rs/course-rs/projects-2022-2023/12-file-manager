#include <catch2/catch.hpp>
#include <QDir>
#include <QDebug>
#include "../headers/mainwindow.h"


TEST_CASE("copyDir","[function]"){

    QString dirPath = QDir::currentPath();
    dirPath = dirPath.left(dirPath.lastIndexOf('/'));
    dirPath += "/test/testFiles/";
    if(!QDir(dirPath).exists()) {
        QDir().mkdir(dirPath);
    }

    QDir dir(dirPath);

    SECTION("MainWindow.copyDir() copies nested directory from origin path sourceDir to destination path destinationDir!"){
        //Arrange
        MainWindow m;


        if(!QDir(dirPath+"/sourceDir").exists()) {
            QDir().mkdir(dirPath+"/sourceDir");
        }

        if(!QDir(dirPath+"/sourceDir/nestedDir").exists()) {
            QDir().mkdir(dirPath+"/sourceDir/nestedDir");
        }

        if(!QDir(dirPath+"/destinationDir").exists()) {
            QDir().mkdir(dirPath+"/destinationDir");
        }

        //Act
        m.copyDir(dirPath+"/sourceDir",dirPath+"/destinationDir",true);
        //Assert
        REQUIRE(QDir(dirPath+"/destinationDir/nestedDir").exists());
    }

    dir.removeRecursively();
}
