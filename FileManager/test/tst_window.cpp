#include <catch2/catch.hpp>
#include <QDir>
#include <QDebug>
#include "../headers/mainwindow.h"
#include "../headers/window.h"

TEST_CASE("setDirectory","[function]"){

    QString dirPath = QDir::currentPath();
    dirPath = dirPath.left(dirPath.lastIndexOf('/'));
    dirPath += "/test/testFiles/";
    if(!QDir(dirPath).exists()) {
        QDir().mkdir(dirPath);
    }

    QDir dir(dirPath);

    SECTION("Window.setDirectory() changes the currently displayed directory of the Window to the one given by the arguments!"){
        //Arrange
        MainWindow m;
        Window *window = m.getLeftWindow();
        QString newPath = dirPath;
        //Act
        window->setDirectory(newPath);
        //Assert
        REQUIRE(window->getDirectory()==newPath);
    }

    SECTION("Window.setDirectory() keeps the currently displayed directory of the Window when the argumented path is /.. (used on returning in root directory)!"){
        //Arrange
        MainWindow m;
        Window *window = m.getLeftWindow();
        QString newPath = "/..";
        QString oldPath = window->getDirectory();
        //Act
        window->setDirectory(newPath);
        //Assert
        REQUIRE(window->getDirectory()==oldPath);
    }

    dir.removeRecursively();
}
