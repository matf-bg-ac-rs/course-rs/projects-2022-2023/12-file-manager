#include <catch2/catch.hpp>
#include <QDir>
#include <QFileInfo>
#include <QDebug>
#include "../headers/propertiesdialog.h"

TEST_CASE("PropertiesDialog constructor", "[constructor]"){

    QString dirPath = QDir::currentPath();
    dirPath = dirPath.left(dirPath.lastIndexOf('/'));
    dirPath += "/test/testFiles/";
    if(!QDir(dirPath).exists()) {
        QDir().mkdir(dirPath);
    }

    QDir dir(dirPath);

    SECTION("PropertiesDialog instance successfully created in case of a single directory selected!"){
        //Arrange
        File *dir = new File(QFileInfo(dirPath));
        SelectedFilesList *list = new SelectedFilesList();
        list->append(*dir);
        QString expectedType = "File folder";
        //Act
        PropertiesDialog *properties = new PropertiesDialog(nullptr, list);
        //Assert
        REQUIRE(properties->getType()==expectedType);
    }

    SECTION("PropertiesDialog instance successfully created in case of a single file selected!"){
        //Arrange
        QFile qfile(dirPath+"file.txt");
        qfile.open(QIODevice::ReadWrite);
        File *file = new File(QFileInfo(dirPath+"file.txt"));
        SelectedFilesList *list = new SelectedFilesList();
        list->append(*file);
        QString expectedType = ".txt file";
        //Act
        PropertiesDialog *properties = new PropertiesDialog(nullptr, list);
        //Assert
        REQUIRE(properties->getType()==expectedType);
    }

    SECTION("PropertiesDialog instance successfully created in case of multiple files selected!"){
        //Arrange
        QFile qfile1(dirPath+"file1.txt");
        qfile1.open(QIODevice::ReadWrite);
        File *file1 = new File(QFileInfo(dirPath+"file1.txt"));
        QFile qfile2(dirPath+"file2.txt");
        qfile2.open(QIODevice::ReadWrite);
        File *file2 = new File(QFileInfo(dirPath+"file2.txt"));
        SelectedFilesList *list = new SelectedFilesList();
        list->append(*file1);
        list->append(*file2);
        QString expectedType = "Multiple files";
        //Act
        PropertiesDialog *properties = new PropertiesDialog(nullptr, list);
        //Assert
        REQUIRE(properties->getType()==expectedType);
    }

    dir.removeRecursively();
}

TEST_CASE("FormatSize","[function]"){


    QString dirPath = QDir::currentPath();
    dirPath = dirPath.left(dirPath.lastIndexOf('/'));
    dirPath += "/test/testFiles/";
    if(!QDir(dirPath).exists()) {
        QDir().mkdir(dirPath);
    }

    QDir dir(dirPath);

    SECTION("PropertiesDialog.formatSize() returns size in Bytes when that is the largest unit of measure."){
        //Arrange
        QFile qfile(dirPath+"file.txt");
        qfile.open(QIODevice::ReadWrite);
        File *file = new File(QFileInfo(dirPath+"file.txt"));
        SelectedFilesList *list = new SelectedFilesList();
        list->append(*file);
        qint64 size = 1;
        QString expectedUnit = "Bytes";
        PropertiesDialog *properties = new PropertiesDialog(nullptr, list);
        //Act
        QString newSize = properties->formatSize(size);
        //Assert
        REQUIRE(expectedUnit == newSize.split(" ").last());
    }

    SECTION("PropertiesDialog.formatSize() returns size in KB (KiloBytes) when that is the largest unit of measure."){
        //Arrange
        QFile qfile(dirPath+"file.txt");
        qfile.open(QIODevice::ReadWrite);
        File *file = new File(QFileInfo(dirPath+"file.txt"));
        SelectedFilesList *list = new SelectedFilesList();
        list->append(*file);
        qint64 size = 1024;
        QString expectedUnit = "KB";
        PropertiesDialog *properties = new PropertiesDialog(nullptr, list);
        //Act
        QString newSize = properties->formatSize(size);
        //Assert
        REQUIRE(expectedUnit == newSize.split(" ").last());
    }

    SECTION("PropertiesDialog.formatSize() returns size in MB (MegaBytes) when that is the largest unit of measure."){
        //Arrange
        QFile qfile(dirPath+"file.txt");
        qfile.open(QIODevice::ReadWrite);
        File *file = new File(QFileInfo(dirPath+"file.txt"));
        SelectedFilesList *list = new SelectedFilesList();
        list->append(*file);
        qint64 size = 1024*1024;
        QString expectedUnit = "MB";
        PropertiesDialog *properties = new PropertiesDialog(nullptr, list);
        //Act
        QString newSize = properties->formatSize(size);
        //Assert
        REQUIRE(expectedUnit == newSize.split(" ").last());
    }

    SECTION("PropertiesDialog.formatSize() returns size in GB (GigaBytes) when that is the largest unit of measure."){
        //Arrange
        QFile qfile(dirPath+"file.txt");
        qfile.open(QIODevice::ReadWrite);
        File *file = new File(QFileInfo(dirPath+"file.txt"));
        SelectedFilesList *list = new SelectedFilesList();
        list->append(*file);
        qint64 size = 1024*1024*1024;
        QString expectedUnit = "GB";
        PropertiesDialog *properties = new PropertiesDialog(nullptr, list);
        //Act
        QString newSize = properties->formatSize(size);
        //Assert
        REQUIRE(expectedUnit == newSize.split(" ").last());
    }

    dir.removeRecursively();
}
