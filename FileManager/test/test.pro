TEMPLATE = app
QT += core gui network charts

CONFIG += c++11

isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR=$$(CATCH_INCLUDE_DIR)
!isEmpty(CATCH_INCLUDE_DIR): INCLUDEPATH *= $${CATCH_INCLUDE_DIR}

isEmpty(CATCH_INCLUDE_DIR): {
    message("CATCH_INCLUDE_DIR is not set, assuming Catch2 can be found automatically in your system")
}



INCLUDEPATH *= "$${PWD}/.."

SOURCES +=     \
        main.cpp \
        tst_file.cpp \
        tst_ftp.cpp \
        tst_http.cpp \
        tst_mainwindow.cpp \
        tst_preview.cpp \
        tst_propertiesdialog.cpp \
        tst_window.cpp \
        tst_zip.cpp \
        ../sources/clipboard.cpp  \
        ../sources/connectiondialog.cpp  \
        ../sources/contextmenu.cpp  \
        ../sources/file.cpp  \
        ../sources/ftp_manager.cpp  \
        ../sources/http_client.cpp  \
        ../sources/mainwindow.cpp  \
        ../sources/preview.cpp  \
        ../sources/propertiesdialog.cpp  \
        ../sources/selectedfileslist.cpp \
        ../sources/window.cpp \
        ../sources/filesystemmodel.cpp


HEADERS += \
    ../headers/clipboard.h  \
    ../headers/connectiondialog.h  \
    ../headers/contextmenu.h  \
    ../headers/file.h  \
    ../headers/ftp_manager.h  \
    ../headers/http_client.h  \
    ../headers/mainwindow.h  \
    ../headers/preview.h  \
    ../headers/propertiesdialog.h  \
    ../headers/selectedfileslist.h \
    ../headers/window.h \
    ../headers/filesystemmodel.cpp

FORMS += \
    ../forms/connectiondialog.ui \
    ../forms/ftp_manager.ui \
    ../forms/mainwindow.ui \
    ../forms/http_client.ui \
    ../forms/propertiesdialog.ui

#### zip
QUAZIPCODEDIR =  $${_PRO_FILE_PWD_}"/../libs/quazip"
ZLIBCODEDIR = $${_PRO_FILE_PWD_}"/../libs"
LIBS += -L$${ZLIBCODEDIR} -lz

INCLUDEPATH += $${QUAZIPCODEDIR}

SOURCES += $$files("$${QUAZIPCODEDIR}/*.cpp", false)
SOURCES += $$files("$${QUAZIPCODEDIR}/*.c", false)
HEADERS += $$files("$${QUAZIPCODEDIR}/*.h", false)



