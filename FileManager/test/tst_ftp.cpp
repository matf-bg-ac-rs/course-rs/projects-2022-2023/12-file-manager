#include <catch2/catch.hpp>
#include "headers/ftp_manager.h"
#include <QDir>
#include <QDebug>
#include <QFileInfo>


TEST_CASE("ftp_manager", "[function]")
{
    SECTION("ftp manager succsessfuly connects to ftp servers and fetches files.txt")
    {

//        arrange
        QString ulaz_adresa = "127.0.0.1";
        int ulaz_port = 21;
        QString ulaz_username="ftp_client";
        QString ulaz_password="nikola11";
        bool ocekivani_izlaz = true;


//        act

        ftp_manager *manager = new ftp_manager(nullptr, ulaz_adresa,ulaz_port,ulaz_username,ulaz_password);
        QByteArray responseData = manager->downloadFileListReply()->readAll();
        QStringList fileList = QString(responseData).split(",");


//        assert
        REQUIRE((fileList.size()>0)==ocekivani_izlaz);

        delete manager;

    }

}





