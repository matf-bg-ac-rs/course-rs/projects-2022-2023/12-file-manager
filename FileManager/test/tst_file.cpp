#include <catch2/catch.hpp>
#include <QDir>
#include <QDebug>
#include "../headers/file.h"

TEST_CASE("Rename","[function]"){

    QString dirPath = QDir::currentPath();
    dirPath = dirPath.left(dirPath.lastIndexOf('/'));
    dirPath += "/test/testFiles/";
    if(!QDir(dirPath).exists()) {
        QDir().mkdir(dirPath);
    }

    QDir dir(dirPath);

    SECTION("File.rename() successfully changes the name of a file, when a file of that name is not existant in the current directory!"){
        //Arrange
        QFile testFile(dirPath+"unrenamed.txt");
        testFile.open(QIODevice::ReadWrite);
        File *file = new File(QFileInfo(dirPath+"unrenamed.txt"));
        QString newName = "renamed.txt";
        //Act
        file->rename(newName);
        //Assert
        REQUIRE(file->getName()==newName);
    }

    SECTION("File.rename() aborts name change when file name already exists in current directory!"){
        //Arrange
        QFile testFile1(dirPath+"unrenamed.txt");
        testFile1.open(QIODevice::ReadWrite);
        QFile testFile2(dirPath+"takenName.txt");
        testFile2.open(QIODevice::ReadWrite);
        File *file = new File(QFileInfo(dirPath+"unrenamed.txt"));
        QString oldName = file->getName();
        QString newName = "takenName.txt";
        //Act
        file->rename(newName);
        //Assert
        REQUIRE(file->getName()==oldName);
    }

    SECTION("File.rename() aborts name change when file name is an empty string!"){
        //Arrange
        QFile testFile(dirPath+"unrenamed.txt");
        testFile.open(QIODevice::ReadWrite);
        File *file = new File(QFileInfo(dirPath+"unrenamed.txt"));
        QString oldName = file->getName();
        QString newName = "";
        //Act
        file->rename(newName);
        //Assert
        REQUIRE(file->getName()==oldName);
    }

    dir.removeRecursively();
}

TEST_CASE("getSize","[function]"){

    SECTION("Size of existing file should be greater than 0"){
        //Arrange
        QString path="../resources/icons/cut.png";
        QFileInfo fi(path);
        File f(fi);

        //Act
        qint64 resultSize=f.getSize();

        //Assert
        REQUIRE(resultSize>0);
    }

    SECTION("Size of non-existing file should be 0"){
        //Arrange
        QString path="../resources/icons/blabla.png";
        QFileInfo fi(path);
        File f(fi);

        //Act
        qint64 resultSize=f.getSize();

        //Assert
        REQUIRE(resultSize==0);
    }

    SECTION("Size of direcotory is sum of the sizes of the directory files"){
        //Arrange
        QString path="../resources/stylesheets/";
        QFileInfo fi(path);
        File f(fi);

        QString lightThemePath="../resources/stylesheets/lightTheme.qss";
        QFileInfo fi1(lightThemePath);
        File f1(fi1);

        QString darkThemePath="../resources/stylesheets/darkTheme.qss";
        QFileInfo fi2(darkThemePath);
        File f2(fi2);
        //Act
        qint64 sumOfFileSize=f1.getSize()+f2.getSize();
        qint64 resultSize=f.getSize();
        //Assert
        REQUIRE(sumOfFileSize==resultSize);
    }

}

TEST_CASE("isImg","[function]"){

    SECTION("Comparison between an image file and another file type"){
        //Arrange
        QString path="../resources/icons/cut.png";
        QFileInfo fi(path);
        File f(fi);

        QString darkThemePath="../resources/stylesheets/darkTheme.qss";
        QFileInfo fi2(darkThemePath);
        File f2(fi2);
        //Act
        bool result=f.isImg();
        //Assert
        REQUIRE(!(f2.isImg())==result);
    }


    SECTION("Comparison between two image files"){
        //Arrange
        QString path="../resources/icons/cut.png";
        QFileInfo fi(path);
        File f(fi);

        QString darkThemePath="../resources/icons/rename.png";
        QFileInfo fi2(darkThemePath);
        File f2(fi2);
        //Act
        bool result=f.isImg();
        //Assert
        REQUIRE((f2.isImg())==result);
    }

}

TEST_CASE("getPath","[function]"){

    SECTION("The directory path length is shorter than a file path from that directory"){
        //Arrange
        QString path="../resources/stylesheets/";
        QFileInfo fi(path);
        File f(fi);

        QString lightThemePath="../resources/stylesheets/lightTheme.qss";
        QFileInfo fi1(lightThemePath);
        File f1(fi1);

        //Act
        qint64 filePathLength=(f1.getPath()).length();
        qint64 directoryLength=(f.getPath()).length();

        //Assert
        REQUIRE(filePathLength>directoryLength);
    }


    SECTION("The path length of existing file is greater than 0 "){
        //Arrange
        QString path="../resources/stylesheets/darkTheme.qss";
        QFileInfo fi(path);
        File f(fi);

        //Act
        QString filePath=f.getPath();
        //Assert
        REQUIRE(filePath.length()>0);
    }


}






