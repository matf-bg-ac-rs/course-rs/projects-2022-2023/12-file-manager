#include <catch2/catch.hpp>
#include "headers/http_client.h"
#include <QDir>
#include <QDebug>
#include <QFileInfo>


TEST_CASE("http client", "[function]")
{
    SECTION("http client succsessfully starts request")
    {

//        arrange
        QString url = "http://www.matf.bg.ac.rs/";

        bool ocekivani_izlaz = true;

//        act

        HTTP_Client *client = new HTTP_Client();
        const QUrl validUrl = QUrl::fromUserInput(url);
        client->startRequest(validUrl);
        bool response = client->getReply()->isRunning();


//        assert
        REQUIRE(response==ocekivani_izlaz);

        delete client;

    }

}





