#include <catch2/catch.hpp>
#include <QDir>
#include <QDebug>
#include "../headers/preview.h"



TEST_CASE("setScene","[function]"){

    SECTION("Scene doesnt exist before call of setScene function"){
        //Arrange
        QGraphicsView* gv=new QGraphicsView();
        Preview* preview=new Preview(gv);
        bool expected=true;
        //Act
        preview->setScene();
        bool result=(preview->getGraphicsView()->scene())? true:false;
        //Assert
        REQUIRE(result==expected);

        delete preview;
        delete gv;
    }


    SECTION("GraphicsView stays visible after call of setScene function"){
        //Arrange
        QGraphicsView* gv=new QGraphicsView();
        Preview* preview=new Preview(gv);
        preview->getGraphicsView()->setVisible(true);
        bool expected=true;
        //Act
        preview->setScene();
        bool result=(preview->getGraphicsView()->isVisible())? true:false;
        // Assert
        REQUIRE(result==expected);

        delete preview;
        delete gv;
    }

}

TEST_CASE("imagePreview","[function]"){

    SECTION("The GraphicsView will be visibile if the file is an image "){
        //Arrange
        QGraphicsView* gv=new QGraphicsView();
        Preview* preview=new Preview(gv);
        QString path="../resources/icons/cut.png";
        QFileInfo fi(path);
        File f(fi);
        bool expected=true;
        //Act
        preview->imagePreview(f);
        bool result=preview->getGraphicsView()->isVisible();
        //Assert
        REQUIRE(result==expected);

        delete preview;
        delete gv;
    }


    SECTION("The GraphicsView will not be visibile if the file isn't an image  "){
        //Arrange
        QGraphicsView* gv=new QGraphicsView();
        Preview* preview=new Preview(gv);
        QString path="../resources/stylesheets/darkTheme.qss";
        QFileInfo fi(path);
        File f(fi);
        bool expected=false;
        //Act
        preview->imagePreview(f);
        bool result=preview->getGraphicsView()->isVisible();
        //Assert
        REQUIRE(result==expected);

        delete preview;
        delete gv;
    }

}


