#include <catch2/catch.hpp>
#include "../headers/file.h"
#include "../headers/selectedfileslist.h"
#include <QDir>
#include <QDebug>
#include <QFileInfo>

// putanja do foldera sa fajlovima za test, dodati ime zeljenog fajla
QString basePath;
// brise sve iz foldera sa fajlovima i pravi novi fajl
void initialization(){
    basePath = QDir::currentPath();
    basePath = basePath.left(basePath.lastIndexOf('/'));
    basePath += "/test/files/";

    if(!QDir(basePath).exists()) {
        QDir().mkdir(basePath);
    }

    QDir dir(basePath);
    for(QString p : dir.entryList()) {
        if (p != "." && p != ".."){
            qDebug() << p;
            QFile* f = new QFile(basePath+p);
            f->moveToTrash();
        }
    }

    QFile newFile(basePath+"1.txt");
    newFile.open(QIODevice::ReadWrite);
    newFile.write("12345");
}

TEST_CASE("Initialization of files", "initial")
{
    initialization();
}


TEST_CASE("Compressing single file in same directory", "SelectedFilesList.compress()")
{
    File* file = new File(QFileInfo(basePath+"1.txt"));

    SelectedFilesList* selectedFilesList = new SelectedFilesList();
    selectedFilesList->append(*file);
    selectedFilesList->compress("test1");

    REQUIRE(QFileInfo::exists(basePath+"test1.zip"));
}
TEST_CASE("Decompressing ZIP file", "SelectedFilesList.extract()")
{
    File* file = new File(QFileInfo(basePath+"test1.zip"));

    SelectedFilesList* selectedFilesList = new SelectedFilesList();
    selectedFilesList->append(*file);
    selectedFilesList->extract();

    REQUIRE(QFileInfo::exists(basePath+"/test1/1.txt"));
}
TEST_CASE("Compare original and extracted file", "compare")
{
    QFile original(basePath+"1.txt");
    QFile extracted(basePath+"test1/1.txt");
    bool check = true;

    if (!original.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << "text1.txt file can't be opened...";
        check = false;
    }

    if (!extracted.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << "text2.txt file can't be opened...";
        check = false;
    }


    QTextStream in1(&original), in2(&extracted);

    while ( !in1.atEnd() && !in2.atEnd() ) {
        QString l1 = in1.readLine();
        QString l2 = in2.readLine();
        if ( l1 != l2 )
            check = false;
    }
    REQUIRE(check);
}




















