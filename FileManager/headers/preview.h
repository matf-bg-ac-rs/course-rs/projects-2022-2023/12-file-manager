#ifndef PREVIEW_H
#define PREVIEW_H

#include <QtCharts>

#include "selectedfileslist.h"
#include "window.h"

class Preview : public QObject
{
    Q_OBJECT
public:
    Preview(QGraphicsView *graphicsView);
    ~Preview() = default;

    QGraphicsView *getGraphicsView();
    void setScene();
    void imagePreview(const File file);
    QChart* displayPieChart( const SelectedFilesList* leftList, const SelectedFilesList* rightList);

private:
    QMap<QString,int> extensionsMap( const SelectedFilesList* list);
    void processFile(const File file, QMap<QString, int> &extMap) const;
    void processDirectory(const QString path, QMap<QString,int>& extMap) const;

    QGraphicsView *m_graphicsView;
    qint64 m_w;
    qint64 m_h;
};

#endif // PREVIEW_H
