#ifndef SELECTEDFILESLIST_H
#define SELECTEDFILESLIST_H

#include "headers/file.h"
#include <QList>
#include <QMenu>

enum FilesType
{
    SINGLE_FILE,
    MULTIPLE_FILES,
    SINGLE_DIRECTORY,
    MULTIPLE_DIRECTORIES,
    ZIPPED_FILE,
    NO_FILE
};

class SelectedFilesList : public QList<File>
{
public:
    SelectedFilesList() = default;

    QString to_string() const;
    std::list<File> toStdList() const;

    FilesType getFilesType() const;
    void properties() const;

    // pozivaju QDialog
    void prepareExtract(QMenu* contextMenu) const;
    void prepareCompress(QMenu* contextMenu) const;
    void prepareExtractTo(QMenu* contextMenu) const;
    void prepareCompressTo(QMenu* contextMenu) const;

    void compress(QString newFileName, QString path = "") const;
    void extract(QString path = "") const;

    static void recurseAddDir(QDir d, QStringList &list);
    static bool archive(const QString & filePathOfZip, QList<File> listOfSelectedFiles);
};

#endif // SELECTEDFILESLIST_H
