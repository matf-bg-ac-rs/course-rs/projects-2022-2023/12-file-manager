#ifndef FTP_MANAGER_H
#define FTP_MANAGER_H

#include <QWidget>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>
#include <QFileInfo>
#include <QFileDialog>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QStandardItemModel>

namespace Ui {
class ftp_manager;
}

class ftp_manager : public QWidget
{
    Q_OBJECT

public:
    explicit ftp_manager(QWidget *parent = nullptr, const QString &ftpAddress = "127.0.0.1", int ftpPort= 21, const QString &username="", const QString &password="");
    ~ftp_manager();

    void getFileList();


    QNetworkReply *downloadFileListReply() const;

private slots:
    void on_pbOpen_clicked();
    void on_pbUpload_clicked();
    void on_pbSetFolder_clicked();
    void on_lwFileList_itemDoubleClicked(QListWidgetItem *item);

    void downloadFileListFinished();
    void uploadFileListFinished();

    void uploadFileProgress(qint64 bytesSent, qint64 bytesTotal);
    void uploadFileFinished();

    void downloadFileProgress(qint64 byteReceived,qint64 bytesTotal);
    void downloadFileFinished();

private:
    Ui::ftp_manager *ui;
    QNetworkAccessManager* m_manager;

    QString m_ftpAddress;
    int m_ftpPort;
    QString m_username;
    QString m_password;

    QNetworkReply* m_downloadFileListReply;
    QNetworkReply* m_uploadFileListReply;

    QNetworkReply* m_uploadFileReply;
    QNetworkReply* m_downloadFileReply;

    QStringList m_fileList;
    QString m_uploadFileName;
    QString m_downloadFileName;
};

#endif // FTP_MANAGER_H
