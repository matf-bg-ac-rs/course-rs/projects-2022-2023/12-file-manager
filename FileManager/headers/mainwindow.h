#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "headers/window.h"
#include "headers/connectiondialog.h"
#include "headers/propertiesdialog.h"
#include "headers/clipboard.h"
#include "headers/preview.h"
#include "headers/http_client.h"

#include <QCloseEvent>
#include <QSettings>
#include <QMessageBox>
#include <QInputDialog>
#include <QDebug>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    static bool copyDir(const QString sourceDir, const QString destinationDir, const bool overWriteDirectory);

    Clipboard *getClipboard() const;
    bool isOpenWithFunctionEnabled() const;

    Window *getLeftWindow();
    Window *getRightWindow();

public slots:
    void toggle_ShowHiddenFilesAndDirectories();
    void toggle_alternatingRowColors();
    void toggle_darkMode();
    void toggle_imagePreview();
    void toggle_openWithFunction();

    void changeDragAndDropAction();

    void on_FocusChanged(QWidget * old, QWidget * now);

    void on_pbNewFolder_clicked();
    void on_pbViewClicked();

    void on_actionNew_connection_triggered();
    void on_actionHTTP_Client_triggered();

    void openTerminal();
    void copyFiles();
    void pasteFiles();
    void moveFiles();
    void deleteFiles(bool deleteBetweenCutAndPaste);

private:
    void keyPressEvent(QKeyEvent *e);
    void closeEvent(QCloseEvent *event);
    void showEvent(QShowEvent *event);

    void clickedTvLeft(const QModelIndex &index);
    void clickedTvRight(const QModelIndex &index);
    void clickedWindow(const QModelIndex &index,Window *window);

    bool m_openWithFunctionEnabled;

    Ui::MainWindow *m_ui;

    Window *m_leftWindow;
    Window *m_rightWindow;

    bool m_lastClickedWindow;//false za levi prozor,true za desni

    bool m_isMove;
    Clipboard* m_clipboard;
    QString m_pathCopiedFrom;
    QList<File> m_filesToDelete;

    QSettings m_settings;

    ConnectionDialog *m_ConnectionDialog;

    Preview *m_preview;
    bool m_viewEnabled;

    DragAndDropAction m_dragAndDropAction;

    HTTP_Client *m_HTTP_Client;

};
#endif // MAINWINDOW_H
