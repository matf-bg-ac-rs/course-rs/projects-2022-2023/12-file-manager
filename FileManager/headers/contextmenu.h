#ifndef CONTEXTMENU_H
#define CONTEXTMENU_H

#include "headers/selectedfileslist.h"
#include "headers/window.h"

#include <QMenu>
#include <QtGlobal>
#include <QString>
#include <QByteArray>
#include <QProcess>

class ContextMenu : public QMenu
{
public:
    ContextMenu(Window *parent, const QPoint& position, SelectedFilesList *list);
    ~ContextMenu();

    void execute();

    void addSingleFileActions();
    void addMultipleFilesActions();
    void addSingleDirectoryActions();
    void addMultipleDirectoriesActions();
    void addZippedFileActions();
    void addNoFileActions();

    void addOpenAction();
    void addOpenWithAction();
    void addCopyAction();
    void addCutAction();
    void addPasteAction();
    void addRenameAction();
    void addCompressAction();
    void addExtractAction();
    void addCompressToAction();
    void addExtractToAction();
    void addDeleteAction();
    void addPropertiesAction();
    void addNewFolderAction();
    void addOpenTerminalAction();

    void insertSeparator();

public slots:
    void openWith();
    void open() const;
    void rename();
    void properties() const;
    void newFolder() const;
    void copyFiles() const;
    void pasteFiles() const;
    void deleteFiles() const;

    void extract();
    void compress();
    void extractTo();
    void compressTo();

    void moveFiles();

    void openTerminal() const;

private:
    bool openWithFunctionIsEnabled();

    Window *m_parent;
    const QPoint m_position;
    SelectedFilesList *m_files;
    QList<QAction*> m_actionList;
};


#endif // CONTEXTMENU_H
