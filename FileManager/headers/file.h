#ifndef FILE_H
#define FILE_H

#include <QString>
#include <QDateTime>
#include <QFile>
#include <QFileInfo>

#include <QFileDialog>
#include <QUrl>
#include <QProcess>


class File
{
public:
    static const QList<QString> zipExtensions;
    static const QList<QString> imgExtensions;

    File(const QString name, const QString path, const qint64 size,
         const QString extension,
         const QDateTime created, const QDateTime modified,
         const QFileDevice::Permissions permissions,
         const bool isDir,
         const QDateTime opened);

    File(const QFileInfo &fileInfo);

    ~File() = default;

    bool operator==(const File &f) const;
    QString getName() const;
    bool isDir() const;
    bool isZip() const;
    bool isImg() const;


    void open();
    void openWith(const QString &program);

    QString getPath() const;

    bool rename(const QString &newName);
    bool setPermissions(const QFileDevice::Permissions permissions);

    QString getExtension() const;
    qint64 getSize() const;
    QDateTime getCreated() const;
    QDateTime getModified() const;
    QFileDevice::Permissions getPermissions() const;
    QDateTime getOpened() const;
    QPair<qint64,qint64> getContents() const;

private:
    QString m_name;
    QString m_path;
    qint64 m_size;
    QString m_extension;
    QDateTime m_created;
    QDateTime m_modified;
    QFileDevice::Permissions m_permissions;
    bool m_isDir;

    QDateTime m_opened;

};
#endif // FILE_H
