#ifndef HTTP_CLIENT_H
#define HTTP_CLIENT_H

#include <QDialog>
#include <QProgressDialog>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrl>
#include <memory>
#include <QFile>

namespace Ui {
class HTTP_Client;
}

class HTTP_Client : public QDialog
{
    Q_OBJECT

public:
    explicit HTTP_Client(QWidget *parent = nullptr);
    ~HTTP_Client();

    void startRequest(const QUrl &requestedUrl);


    QNetworkReply *getReply() const;

private slots:
    void downloadFile();
    void cancelDownload();
    void httpFinished();
    void httpReadyRead();
    void enableDownloadButton();
    void chooseDirectory();

private:
    Ui::HTTP_Client *ui;
    std::unique_ptr<QFile> openFileForWrite(const QString &fileName);

    QUrl m_url;
    QNetworkAccessManager m_qnam;
    QNetworkReply* m_reply;
    std::unique_ptr<QFile> m_file;
    bool m_httpRequestAborted;
};

#endif // HTTP_CLIENT_H
