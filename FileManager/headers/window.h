#ifndef WINDOW_H
#define WINDOW_H

#include "headers/selectedfileslist.h"
#include "headers/clipboard.h"
#include "headers/filesystemmodel.h"

#include <QTableView>

enum DisplayedContentsColumn{
    NAME,
    SIZE,
    TYPE,
    DATE
};

class Window : public QWidget
{
    Q_OBJECT

public:
    Window(QWidget *parent, FileSystemModel *fileSystemModel, QTableView *tableView, QLineEdit *lePath);

    QString getDirectory() const;

    void setDirectory(const QString directory);

    void search();

    void on_activated(const QModelIndex &index);
    void on_mousePressed(const QModelIndex &index);
    void toggle_ShowHiddenFilesAndDirectories();
    void toggle_alternatingRowColors(const bool toggle);

    SelectedFilesList *getSelectedFilesList();
    FileSystemModel *getModel();

    QWidget* getParent();

    SelectedFilesList *selectedFilesList() const;
    Clipboard *getClipboard();

public slots:
    void showContextMenu(const QPoint& pos);
    void on_selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

private:
    QWidget *m_parent;
    FileSystemModel *m_model;
    QTableView *m_view;
    QLineEdit *m_lePath;
    QString m_directory;

    QDir::Filters m_filters;
    SelectedFilesList *m_selectedFilesList;

};

#endif // WINDOW_H
