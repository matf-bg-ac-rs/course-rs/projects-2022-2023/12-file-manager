#ifndef FILESYSTEMMODEL_H
#define FILESYSTEMMODEL_H

#include <QFileSystemModel>

enum DragAndDropAction
{
    COPY_ACTION,
    MOVE_ACTION
};

class FileSystemModel : public QFileSystemModel
{
public:
    FileSystemModel(QObject *parent, DragAndDropAction *action);

    Qt::ItemFlags flags(const QModelIndex &index) const;

    bool canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);

private:
    bool targetContainsSource_or_targetIsCurrentDirectory(const QString source, QString targetPath) const;

    bool moveFile(QFile *file, const QString targetPath);
    bool copyFile(QFile *file, const QString targetPath);
    bool removeFile(const QString path);

    QObject *m_parent;
    DragAndDropAction *m_action;
};

#endif // FILESYSTEMMODEL_H
