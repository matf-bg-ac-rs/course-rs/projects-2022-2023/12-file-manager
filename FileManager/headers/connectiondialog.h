#ifndef CONNECTIONDIALOG_H
#define CONNECTIONDIALOG_H

#include <QDialog>
#include "ftp_manager.h"
namespace Ui {
class ConnectionDialog;
}

class ConnectionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConnectionDialog(QWidget *parent = nullptr);
    ~ConnectionDialog();

private slots:
    void on_pbConnection_clicked();

private:
    Ui::ConnectionDialog *ui;

    ftp_manager *m_ftpManager;
};

#endif // CONNECTIONDIALOG_H
