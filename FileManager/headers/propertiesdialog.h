#ifndef PROPERTIESDIALOG_H
#define PROPERTIESDIALOG_H

#include <QDialog>
#include "selectedfileslist.h"

namespace Ui {
class PropertiesDialog;
}

class PropertiesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PropertiesDialog(QWidget *parent = nullptr, const SelectedFilesList *selectedFilesList = nullptr);
    ~PropertiesDialog();

    QString formatSize(qint64 size) const;

    void displayPieChart();

    QString getType();

private slots:
    void on_pbCancel_clicked();
    void on_pbOk_clicked();
    void on_btApply_clicked();

private:
    Ui::PropertiesDialog *ui;

    const SelectedFilesList *m_selectedFilesList;
};

#endif // PROPERTIESDIALOG_H
