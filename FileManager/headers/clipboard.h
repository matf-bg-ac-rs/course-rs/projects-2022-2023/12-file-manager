#ifndef CLIPBOARD_H
#define CLIPBOARD_H

#include "selectedfileslist.h"

class Clipboard
{
public:
    Clipboard();

    void clear();
    void set(const SelectedFilesList *list);

    bool isEmpty() const;
    QString to_string() const;
    std::list<File> toStdList() const;

private:
    SelectedFilesList *m_list;
};

#endif // CLIPBOARD_H
