# :file_folder: File Manager :

[![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-blue)](https://www.cplusplus.com/)
[![qt5](https://img.shields.io/badge/Framework-Qt5-green)](https://doc.qt.io/qt-5/) <br>


## :memo: Opis :
Program se koristi u svrhu jednostavnijeg baratanja fajlovima na sistemu. Glavni prozor ima dva potprozora koji predstavljaju sadrzaj nekog direktorijuma. Ako se u prozorima nalaze sadrzaji dva razlicita direktorijuma u fajl sistemu, omoguceno je jednostavnije obavljanje operacije kopiranja, premestanja, ekstraktovanja iz arhiva i slicno. Sa druge strane, program korisniku omogucava upravljanje pojedinacnim, ili odabranom grupom fajlova u tekucem direktorijumu.


## Korišćeno okruženje:
- Qt5

## :books: Korišćene biblioteke:

- Qt 5.15_2
- QtCharts
- QuaZip
- ZLib
- Catch2

## :hammer: Instalacija potrebnih biblioteka:
- [Catch2](https://github.com/catchorg/Catch2)
- [Qt i Qt Creator](https://www.qt.io/download)
- Preuzimanje ZLib biblioteke preko terminala pomoću komande: `sudo apt install zlib1g-dev`
- Preuzimanje QtChart biblioteke preko terminala pomoću komande: `sudo apt install libqt5charts5-dev`.<br> 
  Ukoliko postoji problem pri prevođenju zbog ove biblioteke preporučuje se instalacija pomenute biblioteke korišćenjem Qt Maintenance Tool-a. 

## Preuzimanje

- Preuzimanje programa **FileManager** je omogućeno preko terminala pomoću komande `git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2022-2023/12-file-manager.git`

## Pokretanje programa :
### Pomoću QtCreator-a
-Otvorite FileManager.pro fajl a zatim pritisnite na na dugme **Run**(`Ctrl+R`) u donjem levom uglu.

## :movie_camera: Demo snimak :
- [Link](https://drive.google.com/file/d/1bGGdZnsuqalGzmO0rzxVZXhr1CCDaHXd/view?usp=share_link)

<hr>
<ul>
    <li><a href="https://gitlab.com/vladjov00">Vladimir Jovanović 96/2019</a></li>
    <li><a href="https://gitlab.com/jormundur00">Jovan Vukićević 90/2019</a></li>
    <li><a href="https://gitlab.com/SrdjanKatancevic">Srđan Katančević 268/2018</a></li>
    <li><a href="https://gitlab.com/DjoleXPet">Đorđe Petrović 113/2019</a></li>
    <li><a href="https://gitlab.com/dicedice1">Nikola Markov 215/2018</a></li>
    <li><a href="https://gitlab.com/erminskrijelj">Ermin Škrijelj 194/2018</a></li>
</ul>
