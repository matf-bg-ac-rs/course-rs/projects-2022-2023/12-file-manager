Selektovanje fajlova i ocitavanje informacija:

Kratak opis: Korisnik selektuje jedan ili vise fajlova u jednom od prozora i pritiska desni klik. Aplikacija otvara kontekstni
meni sa opcijama (copy, move, delete, compress, extract, rename, properties...).
Korisnik bira opciju "Properties". Aplikacija otvara prozor u kojem prikazuje informacije o selektovanim fajlovima*.

*Napomena: pod "fajl" se podrazumevaju svi tipovi fajlova, ukljucujuci i direktorijume.

Akteri: Korisnik

Preduslovi: Aplikacija je pokrenuta i prikazan je glavni prozor.

Postuslovi: Sadrzaj dva prozora koji prikazuju fajlove nije promenjen (osim u slucaju promene imena fajla).

Osnovni tok:
	1. Korisnik selektuje jedan ili vise fajlova u jednom od prozora. Selektovanje se moze odraditi na vise nacina:

		- Korisnik pritiska levi klik na jedan fajl

		- Korisnik pritiska SHIFT+levi klik na jedan fajl (A), selektujuci sve fajlove koji se nalaze izmedju najblizeg selektovanog
		  fajla (B) koji se u prozoru nalazi iznad fajla koji je pritisnut (A) i samog tog fajla (B), ukljucujuci i pritisnut fajl 
		  (A). Ukoliko takav fajl (B) ne postoji, selektuju se svi fajlovi koji se nalaze u iznad fajla koji je pritisnut (A),
		  ukljucujuci i pritisnut fajl (A).

		- Korisnik drzi CTRL dugme i pritiska levim klikom na razlicite fajlove, selektujuci svaki od njih.

	2. Aplikacija dodaje selektovane fajlove u listu selektovanih fajlova.

	3. Korisnik pritiska desni klik na jednom od selektovanih fajlova.

	4. Aplikacija proverava listu selektovanih fajlova i otvara prozor sa opcijama:
		
		4.1. Ukoliko je selektovan jedan regularan fajl, koji nije kompresovan fajl:
		
			4.1.1. Aplikacija u prozoru sa opcijama prikazuje opcije OPEN, COPY, CUT, PASTE, COMPRESS, COMPRESS TO, RENAME, DELETE, 
				   PROPERTIES

			4.1.2. Prelazi se na korak 5

		4.2. Ukoliko je selektovan jedan regularan fajl, koji jeste kompresovan (.zip) fajl:
		
			4.2.1. Aplikacija u prozoru sa opcijama prikazuje opcije OPEN, COPY, CUT, PASTE, COMPRESS, COMPRESS TO..., EXTRACT, 
				   EXTRACT TO..., RENAME, DELETE, PROPERTIES

			4.2.2. Prelazi se na korak 5

		4.3. Ukoliko je selektovan jedan direktorijum:

			4.3.1. Aplikacija u prozoru sa opcijama prikazuje opcije OPEN, COPY, CUT, PASTE, COMPRESS, COMPRESS TO..., DELETE, 
				   PROPERTIES, OPEN IN TERMINAL

			4.3.2. Prelazi se na korak 5

		4.4. Ukoliko je selektovano vise od jednog fajla:

			4.4.1. Aplikacija u prozoru sa opcijama prikazuje opcije COPY, CUT, PASTE, COMPRESS, COMPRESS TO..., DELETE, PROPERTIES

			4.4.2. Prelazi se na korak 5
 
	5. Korisnik pritiska na opciju PROPERTIES

	6. Aplikacija otvara novi prozor PROPERTIES, u kojem je otvoren potprozor INFO:

		6.1. Ukoliko je selektovan jedan fajl, koji nije direktorijum:

			6.1.1. Aplikacija u prozoru prikazuje informacije o imenu fajla, tipu, velicini, kada je fajl napravljen, izmenjen, kao i 
					kada je poslednji put pristupljeno fajlu.
					Prozor sadrzi dva potprozora: informacije o selektovanim fajlovima (INFO) i dozvole pristupa (PERMISSIONS).

			6.1.2. Ukoliko korisnik pritisne na potprozor PERMISSIONS:
		
				6.1.2.1. Prelazi se na korak 7.

			6.1.3. Ukoliko korisnik pritisne na neko od dugmica X, OK, ili CANCEL:

				6.1.3.1. Prelazi se na korak 8.

		6.2. Ukoliko je selektovan jedan fajl, koji jeste direktorijum:

				6.2.1. Aplikacija u prozoru prikazuje informacije o imenu fajla, tipu, velicini, kada je fajl napravljen, izmenjen,
					kada je poslednji put pristupljeno fajlu, kao i informaciju o tome koliko fajlova i direktorijuma sadrzi.
					Pored toga, u prozoru se iscrtava dijagram koji daje informaciju o tome koji tipovi fajlova se nalaze u 
					direktorijumu, na osnovu ekstenzije fajlova.
					Prozor sadrzi dva potprozora: informacije o selektovanim fajlovima (INFO) i dozvole pristupa (PERMISSIONS).

				6.2.2. Ukoliko korisnik pritisne na potprozor PERMISSIONS:
		
					6.2.2.1. Prelazi se na korak 7.

				6.2.3. Ukoliko korisnik pritisne na neko od dugmica X, OK, ili CANCEL:

					6.2.3.1. Prelazi se na korak 8.

		6.3. Ukoliko je selektovano vise fajlova:

				6.3.1. Aplikacija u prozoru prikazuje informacije o lokaciji (direktorijum u kojem se selektovani fajlovi nalaze),
						broju fajlova koji su selektovani (ukoliko je neki fajl direktorijum, na broj fajlova se dodaje broj fajlova
						koji taj direktorijum sadrzi), kao i informaciju o ukupnoj velicini selektovanih fajlova.
						Pored toga, u prozoru se iscrtava dijagram koji daje informaciju o tome koji tipovi fajlova se nalaze medju
						selektovanim fajlovima, na osnovu ekstenzije fajlova.
						Prozor sadrzi dva potprozora: informacije o selektovanim fajlovima (INFO) i dozvole pristupa (PERMISSIONS).

				6.3.2. Ukoliko korisnik pritisne na potprozor PERMISSIONS:
		
					6.3.2.1. Prelazi se na korak 7.

				6.3.3. Ukoliko korisnik pritisne na neko od dugmica X, OK, ili CANCEL:

					6.3.3.1. Prelazi se na korak 8.


	7. Aplikacija otvara prozor PERMISSIONS:

		7.1. Korisnik menja prava pristupa selektovanih fajlova.

		7.2. Aplikacija cuva podesavanja.

		7.3. Ukoliko korisnik pritisne na potprozor INFO:

			7.3.1. Prelazi se na korak 6.1.

		7.4. Ukoliko korisnik pritisne na neko od dugmica X, OK, ili CANCEL:

			7.4.1. Prelazi se na korak 8.

	8. Aplikacija zatvara prozor PROPERTIES.

	9. Korisnik nastavlja sa koriscenjem aplikacije.

Alternativni tokovi: Neocekivani izlaz iz aplikacije. Ukoliko korisnik iskljuci aplikaciju, aplikacija završava sa svojim radom. 
Slucaj upotrebe se zavrsava.

Podtokovi: /

Specijalni zahtevi: /

Dodatne informacije: Aplikacija nakon zatvaranja prozora PROPERTIES ne menja listu selektovanih fajlova.

Dodatne informacije: /